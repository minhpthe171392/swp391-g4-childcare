<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><!-- comment -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><!-- comment -->
    <meta charset="UTF-8">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Bin-It">
    <meta property="og:url" />
    <meta property="og:type" content="truongbinit" />
    <meta property="og:title" content="Website TruongBin" />
    <meta property="og:description" content="Wellcome to my Website" />

    <title></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="/style.css">
    <!-- Latest compiled and minified CSS -->
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <!-- jQuery library -->
    <!--===============================================================================================-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <!--===============================================================================================-->
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
    <!--===============================================================================================-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  
</head>

<body onload="time()">
    <div class="container-fluid al">
        <div id="clock"></div>
        <Br>
        <p>LIST OF APPOINTMENT</p><Br><Br>
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Enter the name"
            data-toggle="tooltip" data-placement="bottom" title="Search a list of people who book an appointment">
        <i class="fa fa-search" aria-hidden="true"></i>
        <button class="nv btn add-new" type="button" data-toggle="tooltip" data-placement="top"
            title="Add New Patient"><i class="fas fa-user-plus"></i></button>
        <button class="nv" type="button" onclick="sortTable()" data-toggle="tooltip" data-placement="top"
            title=""><i class="fa fa-filter" aria-hidden="true"></i></button>
        <button class="nv cog" data-toggle="tooltip" data-placement="bottom" title=""><i
                class="fas fa-cogs"></i></button>
        <div class="table-title">
            <div class="row">

            </div>

        </div>
        <table class="table table-bordered" id="myTable">
            <thead>
                <tr class="ex">
                    <th width="auto">Patient Name</th>
                    <th width="auto">Slot</th>
                    <th>Appointment Date</th>
                    <th>Nurse ID</th>
                </tr>
            </thead>
           <c:forEach items="${listI}" var="o">
            <tbody>
            
                <tr>
                    <td>${o.reservation_name}</td>
                    <td>${o.invoice_doctor_schedule_id}</td>
                    <td>${o.date_and_time}</td>
                    <td>${o.receptionist_id}</td>
                </tr>
            </tbody>
            </c:forEach>
        </table>
                <div id="pageNavPosition" class="text-right"></div>
                <script type="text/javascript">
                    var pager = new Pager('myTable', 5);
                    pager.init();
                    pager.showPageNav('pager', 'pageNavPosition');
                    pager.showPage(1);
                </script>
            </div>
        <script src="jquery.min.js"></script>
        <script type="text/javascript">
            function myFunction() {
                var input, filter, table, tr, td, i, txtValue;
                input = document.getElementById("myInput");
                filter = input.value.toUpperCase();
                table = document.getElementById("myTable");
                tr = table.getElementsByTagName("tr");
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[0];
                    if (td) {
                        txtValue = td.textContent || td.innerText;
                        if (txtValue.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
            }
            function sortTable() {
                var table, rows, switching, i, x, y, shouldSwitch;
                table = document.getElementById("myTable");
                switching = true;
                while (switching) {
                    switching = false;
                    rows = table.rows;
                    for (i = 1; i < (rows.length - 1); i++) {
                        shouldSwitch = false;
                        x = rows[i].getElementsByTagName("TD")[0];
                        y = rows[i + 1].getElementsByTagName("TD")[0];
                        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                            shouldSwitch = true;
                            break;
                        }
                    }
                    if (shouldSwitch) {
                        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                        switching = true;
                        swal("ThÃ nh CÃ´ng!", "Báº¡n ÄÃ£ Lá»c ThÃ nh CÃ´ng", "success");
                    }
                }
            }
            //Thá»i Gian
            function 

            //ThÃªm Báº£ng
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                var actions = $("table td:last-child").html();
                // Append table with add row form on add new button click
                $(".add-new").click(function () {
                    $(this).attr("disabled", "disabled");
                    var index = $("table tbody tr:last-child").index();
                    var row = '<tr>' +
                        '<td><input type="text" class="form-control" name="name" id="name" placeholder="Nháº­p TÃªn"></td>' +
                        '<td><input type="text" class="form-control" name="gioitinh" id="gioitinh" placeholder="Nháº­p Giá»i TÃ­nh"></td>' +
                        '<td><input type="text" class="form-control" name="namsinh" id="namsinh" value="" placeholder="Nháº­p NgÃ y Sinh"></td>' +
                        '<td><input type="text" class="form-control" name="diachi" id="diachi" value="" placeholder="Nháº­p Äá»a Chá»"></td>' +
                        '<td><input type="text" class="form-control" name="chucvu" id="chucvu" value="" placeholder="Nháº­p Chá»©c Vá»¥"></td>' +
                        '<td>' + actions + '</td>' +
                        '</tr>';
                    $("table").append(row);
                    $("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
                    $('[data-toggle="tooltip"]').tooltip();
                });
                // Delete HÃ ng
                $(document).on("click", ".delete", function () {
                    $(this).parents("tr").remove();
                    swal("ThÃ nh CÃ´ng!", "Báº¡n ÄÃ£ XÃ³a ThÃ nh CÃ´ng", "success");
                    $(".add-new").removeAttr("disabled");
                });
            });

            jQuery(function () {
                jQuery(".cog").click(function () {
                    swal("Sorry!", "TÃ­nh NÄng NÃ y ChÆ°a CÃ³", "error");
                });
            });
        </script>
        <!--Tooltip-->
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
</body>

</html>