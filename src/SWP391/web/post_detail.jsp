
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Category" %>
<%@page import="model.User" %>
<%@page import="dao.userDAO.LoginDAO" %>
<%@page import="model.Comments" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>MEDINOVA - Post</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free HTML Templates" name="keywords">
        <meta content="Free HTML Templates" name="description">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&family=Roboto:wght@400;700&display=swap" rel="stylesheet">  

        <!-- Icon Font Stylesheet -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Libraries Stylesheet -->
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

        <!-- Customized Bootstrap Stylesheet -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    
        <body>
        <%User user=(User)session.getAttribute("user");%>
        <!-- Topbar & Navbar Start -->
        <%@include file="navigate.jsp" %>
        <!-- Topbar & Navbar End -->


        <!-- template start -->
        <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <meta name="description" content="" />
            <meta name="author" content="" />
            <title>Blog Post - Start Bootstrap Template</title>
            <!-- Favicon-->
            <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
            <!-- Core theme CSS (includes Bootstrap)-->
            <link href="css/styles.css" rel="stylesheet" />
        </head>
        <body>
            <!-- Responsive navbar-->

            <!-- Page content-->

            <div class="container mt-5">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- Post content-->
                        <article>
                            <!-- Post header-->
                            <header class="mb-4">
                                <!-- Post title-->
                                <h1 class="fw-bolder mb-1">${detail.post_content}</h1>
                                <!-- Post meta content-->
                                <div class="text-muted fst-italic mb-2">${detail.post_created_date}</div>
                                <!-- Post categories-->
                                <a class="badge bg-secondary text-decoration-none link-light" href="#!"></a>
                                <a class="badge bg-secondary text-decoration-none link-light" href="#!"></a>
                            </header>
                            <!-- Preview image figure-->
                            <figure class="mb-4"><img class="img-fluid rounded" src="${detail.post_souce_url}" alt="..." /></figure>
                            <!-- Post content-->
                            <section class="mb-5">
                                <p class="fs-5 mb-4">${detail.post_description}</p>
                                <p class="fs-5 mb-4">The universe is large and old, and the ingredients for life as we know it are everywhere, so there's no reason to think that Earth would be unique in that regard. Whether of not the life became intelligent is a different question, and we'll see if we find that.</p>
                                <p class="fs-5 mb-4">If you get asteroids about a kilometer in size, those are large enough and carry enough energy into our system to disrupt transportation, communication, the food chains, and that can be a really bad day on Earth.</p>
                                <h2 class="fw-bolder mb-4 mt-5">I have odd cosmic thoughts every day</h2>
                                <p class="fs-5 mb-4">For me, the most fascinating interface is Twitter. I have odd cosmic thoughts every day and I realized I could hold them to myself or share them with people who might be interested.</p>
                                <p class="fs-5 mb-4">Venus has a runaway greenhouse effect. I kind of want to know what happened there because we're twirling knobs here on Earth without knowing the consequences of it. Mars once had running water. It's bone dry today. Something bad happened there as well.</p>
                            </section>
                        </article>

                        <!-- Comments section-->
                        <section class="mb-5">
                            <div class="card bg-light">
                                <div class="card-body">
                                    <!-- Comment form-->
                                    <form class="mb-4"  action="postdetail" method="post">
                                        <textarea class="form-control" rows="3" type="text" name="comment" placeholder="Join the discussion and leave a comment!"></textarea>
                                        <input type="submit" value="sent">
                                    </form>
                                    <!-- Comment with nested comments-->

                                    <div class="d-flex mb-4">


                                        <!-- Parent comment-->

                                        <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                        <div class="ms-3">
                                            <div class="fw-bold"></div>

                                            <!--                                         Child comment 1
                                                                                    <div class="d-flex mt-4">
                                                                                        <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                                                                        <div class="ms-3">
                                                                                            <div class="fw-bold">Commenter Name</div>
                                                                                            And under those conditions, you cannot establish a capital-market evaluation of that enterprise. You can't get investors.
                                                                                        </div>
                                                                                    </div>-->
                                            <!-- Child comment 2-->
                                            <!--                                        <div class="d-flex mt-4">
                                                                                        <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                                                                        <div class="ms-3">
                                                                                            <div class="fw-bold">Commenter Name</div>
                                                                                            When you put money directly to a problem, it makes a good headline.
                                                                                        </div>
                                                                                    </div>-->
                                        </div>
                                    </div>

                                    <!-- Single comment-->
                                    <c:forEach items="${list}" var="o">
                                        <div class="d-flex">
                                            <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                            <div class="ms-3">
                                                <div class="fw-bold">Commenter Name</div>
                                                ${o.getComment_text()}
                                            </div>
                                        </div>                
                                    </c:forEach>

                                    </div>
                                </div>
                            </section>
                        </div>
                      

                        <div class="col-lg-4">
                        <!--category-->

                        <div class="card mb-4">
                            <div class="card-header">Category Service</div>
                            <ul class="category-list">
                                <c:forEach items="${listcc}" var="o">
                                    <li class="card-body">
                                        <a href="categorydetail?cid=${o.category_id}" class="category-link">${o.getCategory_name()}</a>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                           
                        <!-- Side widget-->
                        <div class="card mb-4">
                            <div class="card-header">Side Widget</div>
                            <div class="card-body">You can put anything you want inside of these side widgets. They are easy to use, and feature the Bootstrap 5 card component!</div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Bootstrap core JS-->
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
            <!-- Core theme JS-->
            <script src="js/scripts.js"></script>
        </body>
    </html>

    <!-- template end -->
    <!-- Footer Start -->
    <%@include file="footer.jsp" %>
    <!-- Footer End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>

</html>
