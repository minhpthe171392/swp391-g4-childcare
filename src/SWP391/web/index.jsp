
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Category" %>
<%@page import="model.User" %>
<%@page import="dao.PostDAO.PostListDAO" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>MEDINOVA - Hospital Website</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free HTML Templates" name="keywords">
        <meta content="Free HTML Templates" name="description">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&family=Roboto:wght@400;700&display=swap" rel="stylesheet">  

        <!-- Icon Font Stylesheet -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Libraries Stylesheet -->
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

        <!-- Customized Bootstrap Stylesheet -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">

    </head>
    <body>      
        <%User user=(User)session.getAttribute("user");%>
        <%@include file="popup.jsp" %>
        <%@include file="navigate.jsp" %>
        <!-- Hero Start -->
        <div class="container-fluid bg-primary py-5 mb-5 hero-header">
            <div class="container py-5">
                <div class="row justify-content-start">
                    <div class="col-lg-8 text-center text-lg-start">
                        <h5 class="d-inline-block text-primary text-uppercase border-bottom border-5" style="border-color: rgba(256, 256, 256, .3) !important;">Welcome To Medinova</h5>
                        <h1 class="display-1 text-white mb-md-4">Best Healthcare Solution In Your City</h1>
                        <div class="pt-2">
                            <form action="search?index=1" method="post" >
                                <div class="mx-auto" style="width: 100%; padding-right: 100px;">
                                    <div class="input-group">
                                        <select style="width: 20% !important; height: 60px;" class="form-select border-primary w-25" name="option">
                                            <option selected value="0">Filter By</option>
                                            <option value="1">A-Z</option>
                                            <option value="2">Z-A</option>

                                        </select>
                                        <select style="width: 20% !important; height: 60px;" class="form-select border-primary w-25" name="search-type">
                                            <option value="1" selected>Doctor</option>
                                            <option value="2">Service</option>

                                        </select>
                                        <input style="width: 40% !important;" value="${txtSearch}" name="txtSearch" type="text" class="form-control border-primary w-50" placeholder="Keyword">
                                        <button style="width: 20% !important;" class="btn btn-dark border-0 w-25">Search</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->


        <!-- About Start -->
        <!--        <div class="container-fluid py-5">
                    <div class="container">
                        <div class="row gx-5">
                            <div class="col-lg-5 mb-5 mb-lg-0" style="min-height: 500px;">
                                <div class="position-relative h-100">
                                    <img class="position-absolute w-100 h-100 rounded" src="img/about.jpg" style="object-fit: cover;">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="mb-4">
                                    <h5 class="d-inline-block text-primary text-uppercase border-bottom border-5">About Us</h5>
                                    <h1 class="display-4">Best Medical Care For Yourself and Your Family</h1>
                                </div>
                                <p>Tempor erat elitr at rebum at at clita aliquyam consetetur. Diam dolor diam ipsum et, tempor voluptua sit consetetur sit. Aliquyam diam amet diam et eos sadipscing labore. Clita erat ipsum et lorem et sit, sed stet no labore lorem sit. Sanctus clita duo justo et tempor consetetur takimata eirmod, dolores takimata consetetur invidunt magna dolores aliquyam dolores dolore. Amet erat amet et magna</p>
                                <div class="row g-3 pt-3">
                                    <div class="col-sm-3 col-6">
                                        <div class="bg-light text-center rounded-circle py-4">
                                            <i class="fa fa-3x fa-user-md text-primary mb-3"></i>
                                            <h6 class="mb-0">Qualified<small class="d-block text-primary">Doctors</small></h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-6">
                                        <div class="bg-light text-center rounded-circle py-4">
                                            <i class="fa fa-3x fa-procedures text-primary mb-3"></i>
                                            <h6 class="mb-0">Emergency<small class="d-block text-primary">Services</small></h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-6">
                                        <div class="bg-light text-center rounded-circle py-4">
                                            <i class="fa fa-3x fa-microscope text-primary mb-3"></i>
                                            <h6 class="mb-0">Accurate<small class="d-block text-primary">Testing</small></h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-6">
                                        <div class="bg-light text-center rounded-circle py-4">
                                            <i class="fa fa-3x fa-ambulance text-primary mb-3"></i>
                                            <h6 class="mb-0">Free<small class="d-block text-primary">Ambulance</small></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
        <!-- About End -->

        <!-- Services Start -->
        <div class="container-fluid py-5">
            <div class="container">
                <div class="text-center mx-auto mb-5" style="max-width: 500px;">
                    <h1 class="">Excellent Medical Services</h1>
                </div>
                <div class="owl-carousel  team-carousel position-relative text-center mx-auto mb-5 col-md-12" style="max-width: 2500px;">
                    <c:forEach items="${listcc}" var="c">
                        <div class="team-item" >

                            <button value="${c.category_id}" style="border:none;margin-bottom:60px;border-radius:35px;padding:2px;width:300px;" type="button" onclick="getServiceByCategoryID(this)">   
                                <h5 class="d-inline-block text-primary text-uppercase  " style="border-bottom:#13c5dd solid 2px">${c.category_name}</h5>
                            </button>

                        </div>
                    </c:forEach>
                </div>



                <div class="row g-5" id="services"> 

                    <c:forEach items="${services}" var="s">
                        <div class="col-xl-3 col-lg-6" >
                            <div class="bg-light rounded overflow-hidden">
                                <img class="img-fluid w-100" src="${s.image}" style="width: 400px;height:224px;object-fit:cover">
                                <div class="p-4" style="max-height:120px;">
                                    <a class="h3 d-block mb-3" href="postdetail?pid=${s.id }">${s.name}</a>
                                    <p style="color: #1d2a4d;text-align:left;font-weight: 600">
                                        Service ID(${s.id }):
                                        <span style="color: #ff6666; font-size: 20px; font-weight: 600;">${s.price} $</span>
                                    </p>
                                </div>
                                <div class="d-flex justify-content-between border-top p-4">
                                    <div class="d-flex align-items-center">
                                        <img class="rounded-circle me-2" src="img/user.jpg" width="25" height="25" alt="">
                                        <small>${s.price}</small>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <small class="ms-3"><i class="far fa-eye text-primary me-1"></i>12345</small>
                                        <small class="ms-3"><i class="far fa-comment text-primary me-1"></i>123</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

                <script>
                                function getServiceByCategoryID(idBtn) {
                                    var categoryID = idBtn.value;


                                    // Gửi yêu cầu AJAX
                                    $.ajax({
                                        url: "get_service",
                                        method: "POST",
                                        data: {category_id: categoryID},
                                        success: function (response) {
                                            var row = document.getElementById("services");
                                            row.innerHTML = response;
                                        },
                                        error: function (xhr, status, error) {
                                            // Xử lý lỗi (nếu có)
                                            console.log(error);
                                        }
                                    });
                                }

                </script>






            </div>
        </div>

        <!-- Appointment Start -->
        <!--        <div class="container-fluid bg-primary my-5 py-5">
                    <div class="container py-5">
                        <div class="row gx-5">
                            <div class="col-lg-6 mb-5 mb-lg-0">
                                <div class="mb-4">
                                    <h5 class="d-inline-block text-white text-uppercase border-bottom border-5">Appointment</h5>
                                    <h1 class="display-4">Make An Appointment For Your Family</h1>
                                </div>
                                <p class="text-white mb-5">Eirmod sed tempor lorem ut dolores. Aliquyam sit sadipscing kasd ipsum. Dolor ea et dolore et at sea ea at dolor, justo ipsum duo rebum sea invidunt voluptua. Eos vero eos vero ea et dolore eirmod et. Dolores diam duo invidunt lorem. Elitr ut dolores magna sit. Sea dolore sanctus sed et. Takimata takimata sanctus sed.</p>
                                <a class="btn btn-dark rounded-pill py-3 px-5 me-3" href="">Find Doctor</a>
                                <a class="btn btn-outline-dark rounded-pill py-3 px-5" href="">Read More</a>
                            </div>
                            <div class="col-lg-6">
                                <div class="bg-white text-center rounded p-5">
                                    <h1 class="mb-4">Book An Appointment</h1>
                                    <form>
                                        <div class="row g-3">
                                            <div class="col-12 col-sm-6">
                                                <select class="form-select bg-light border-0" style="height: 55px;">
                                                    <option selected>Choose Department</option>
                                                    <option value="1">Department 1</option>
                                                    <option value="2">Department 2</option>
                                                    <option value="3">Department 3</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <select class="form-select bg-light border-0" style="height: 55px;">
                                                    <option selected>Select Doctor</option>
                                                    <option value="1">Doctor 1</option>
                                                    <option value="2">Doctor 2</option>
                                                    <option value="3">Doctor 3</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <input type="text" class="form-control bg-light border-0" placeholder="Your Name" style="height: 55px;">
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <input type="email" class="form-control bg-light border-0" placeholder="Your Email" style="height: 55px;">
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="date" id="date" data-target-input="nearest">
                                                    <input type="text"
                                                           class="form-control bg-light border-0 datetimepicker-input"
                                                           placeholder="Date" data-target="#date" data-toggle="datetimepicker" style="height: 55px;">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="time" id="time" data-target-input="nearest">
                                                    <input type="text"
                                                           class="form-control bg-light border-0 datetimepicker-input"
                                                           placeholder="Time" data-target="#time" data-toggle="datetimepicker" style="height: 55px;">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button class="btn btn-primary w-100 py-3" type="submit">Make An Appointment</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
        <!-- Appointment End -->


        <!-- Pricing Plan Start -->
        <!--        <div class="container-fluid py-5">
                    <div class="container">
                        <div class="text-center mx-auto mb-5" style="max-width: 500px;">
                            <h5 class="d-inline-block text-primary text-uppercase border-bottom border-5">Medical Packages</h5>
                            <h1 class="display-4">Awesome Medical Programs</h1>
                        </div>
                        <div class="owl-carousel price-carousel position-relative" style="padding: 0 45px 45px 45px;">
                            <div class="bg-light rounded text-center">
                                <div class="position-relative">
                                    <img class="img-fluid rounded-top" src="img/price-1.jpg" alt="">
                                    <div class="position-absolute w-100 h-100 top-50 start-50 translate-middle rounded-top d-flex flex-column align-items-center justify-content-center" style="background: rgba(29, 42, 77, .8);">
                                        <h3 class="text-white">Pregnancy Care</h3>
                                        <h1 class="display-4 text-white mb-0">
                                            <small class="align-top fw-normal" style="font-size: 22px; line-height: 45px;">$</small>49<small class="align-bottom fw-normal" style="font-size: 16px; line-height: 40px;">/ Year</small>
                                        </h1>
                                    </div>
                                </div>
                                <div class="text-center py-5">
                                    <p>Emergency Medical Treatment</p>
                                    <p>Highly Experienced Doctors</p>
                                    <p>Highest Success Rate</p>
                                    <p>Telephone Service</p>
                                    <a href="" class="btn btn-primary rounded-pill py-3 px-5 my-2">Apply Now</a>
                                </div>
                            </div>
                            <div class="bg-light rounded text-center">
                                <div class="position-relative">
                                    <img class="img-fluid rounded-top" src="img/price-2.jpg" alt="">
                                    <div class="position-absolute w-100 h-100 top-50 start-50 translate-middle rounded-top d-flex flex-column align-items-center justify-content-center" style="background: rgba(29, 42, 77, .8);">
                                        <h3 class="text-white">Health Checkup</h3>
                                        <h1 class="display-4 text-white mb-0">
                                            <small class="align-top fw-normal" style="font-size: 22px; line-height: 45px;">$</small>99<small class="align-bottom fw-normal" style="font-size: 16px; line-height: 40px;">/ Year</small>
                                        </h1>
                                    </div>
                                </div>
                                <div class="text-center py-5">
                                    <p>Emergency Medical Treatment</p>
                                    <p>Highly Experienced Doctors</p>
                                    <p>Highest Success Rate</p>
                                    <p>Telephone Service</p>
                                    <a href="" class="btn btn-primary rounded-pill py-3 px-5 my-2">Apply Now</a>
                                </div>
                            </div>
                            <div class="bg-light rounded text-center">
                                <div class="position-relative">
                                    <img class="img-fluid rounded-top" src="img/price-3.jpg" alt="">
                                    <div class="position-absolute w-100 h-100 top-50 start-50 translate-middle rounded-top d-flex flex-column align-items-center justify-content-center" style="background: rgba(29, 42, 77, .8);">
                                        <h3 class="text-white">Dental Care</h3>
                                        <h1 class="display-4 text-white mb-0">
                                            <small class="align-top fw-normal" style="font-size: 22px; line-height: 45px;">$</small>149<small class="align-bottom fw-normal" style="font-size: 16px; line-height: 40px;">/ Year</small>
                                        </h1>
                                    </div>
                                </div>
                                <div class="text-center py-5">
                                    <p>Emergency Medical Treatment</p>
                                    <p>Highly Experienced Doctors</p>
                                    <p>Highest Success Rate</p>
                                    <p>Telephone Service</p>
                                    <a href="" class="btn btn-primary rounded-pill py-3 px-5 my-2">Apply Now</a>
                                </div>
                            </div>
                            <div class="bg-light rounded text-center">
                                <div class="position-relative">
                                    <img class="img-fluid rounded-top" src="img/price-4.jpg" alt="">
                                    <div class="position-absolute w-100 h-100 top-50 start-50 translate-middle rounded-top d-flex flex-column align-items-center justify-content-center" style="background: rgba(29, 42, 77, .8);">
                                        <h3 class="text-white">Operation & Surgery</h3>
                                        <h1 class="display-4 text-white mb-0">
                                            <small class="align-top fw-normal" style="font-size: 22px; line-height: 45px;">$</small>199<small class="align-bottom fw-normal" style="font-size: 16px; line-height: 40px;">/ Year</small>
                                        </h1>
                                    </div>
                                </div>
                                <div class="text-center py-5">
                                    <p>Emergency Medical Treatment</p>
                                    <p>Highly Experienced Doctors</p>
                                    <p>Highest Success Rate</p>
                                    <p>Telephone Service</p>
                                    <a href="" class="btn btn-primary rounded-pill py-3 px-5 my-2">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
        <!-- Pricing Plan End -->


        <!-- Team Start -->
        <form>
            <div class="container-fluid py-5">
                <div class="container">
                    <div class="text-center mx-auto mb-5" style="max-width: 500px;">
                        <h5 class="d-inline-block text-primary text-uppercase border-bottom border-5">Our Doctors</h5>
                        <h1 class="display-4">Qualified Healthcare Professionals</h1>
                    </div>
                    <div class="owl-carousel team-carousel position-relative">
                        <c:forEach items="${listdt}" var="o">
                            <div class="team-item">
                                <div class="row g-0 bg-light rounded overflow-hidden">
                                    <div class="col-12 col-sm-5 h-100">
                                        <img src="${o.avatarUrl}" style="object-fit: cover;">
                                    </div>
                                    <div class="col-12 col-sm-7 h-100 d-flex flex-column">
                                        <div class="mt-auto p-4">
                                            <a href="doctor_profile?uid=${o.id}" >${o.name}</a>
                                            <h6 class="fw-normal fst-italic text-primary mb-4">Cardiology Specialist</h6>
                                            <p class="m-0">Contact</p>
                                            <p class="m-0">${o.email}</p>

                                        </div>
                                        <div class="d-flex mt-auto border-top p-4">
                                            <a class="btn btn-lg btn-primary btn-lg-square rounded-circle me-3" href="#"><i class="fab fa-twitter"></i></a>
                                            <a class="btn btn-lg btn-primary btn-lg-square rounded-circle me-3" href="#"><i class="fab fa-facebook-f"></i></a>
                                            <a class="btn btn-lg btn-primary btn-lg-square rounded-circle" href="#"><i class="fab fa-linkedin-in"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </form>
        <!-- Team End -->


        <!--         Search Start 
                <div class="container-fluid bg-primary my-5 py-5">
                    <div class="container py-5">
                        <div class="text-center mx-auto mb-5" style="max-width: 500px;">
                            <h5 class="d-inline-block text-white text-uppercase border-bottom border-5">Find A Doctor</h5>
                            <h1 class="display-4 mb-4">Find A Healthcare Professionals</h1>
                            <h5 class="text-white fw-normal">Duo ipsum erat stet dolor sea ut nonumy tempor. Tempor duo lorem eos sit sed ipsum takimata ipsum sit est. Ipsum ea voluptua ipsum sit justo</h5>
                        </div>
                        <div class="mx-auto" style="width: 100%; max-width: 600px;">
                            <div class="input-group">
                                <select class="form-select border-primary w-25" style="height: 60px;">
                                    <option selected>Department</option>
                                    <option value="1">Department 1</option>
                                    <option value="2">Department 2</option>
                                    <option value="3">Department 3</option>
                                </select>
                                <input type="text" class="form-control border-primary w-50" placeholder="Keyword">
                                <button class="btn btn-dark border-0 w-25">Search</button>
                            </div>
                        </div>
                    </div>
                </div>-->
        <!-- Search End -->


        <!-- Feedback Start -->
        <!--        <div class="container-fluid py-5">
                    <div class="container">
                        <div class="text-center mx-auto mb-5" style="max-width: 500px;">
                            <h5 class="d-inline-block text-primary text-uppercase border-bottom border-5">Feedback</h5>
                            <h1 class="display-4">Patients Say About Our Services</h1>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="owl-carousel testimonial-carousel">
                                    <div class="testimonial-item text-center">
                                        <div class="position-relative mb-5">
                                            <img class="img-fluid rounded-circle mx-auto" src="img/testimonial-1.jpg" alt="">
                                            <div class="position-absolute top-100 start-50 translate-middle d-flex align-items-center justify-content-center bg-white rounded-circle" style="width: 60px; height: 60px;">
                                                <i class="fa fa-quote-left fa-2x text-primary"></i>
                                            </div>
                                        </div>
                                        <p class="fs-4 fw-normal">Dolores sed duo clita tempor justo dolor et stet lorem kasd labore dolore lorem ipsum. At lorem lorem magna ut et, nonumy et labore et tempor diam tempor erat. Erat dolor rebum sit ipsum.</p>
                                        <hr class="w-25 mx-auto">
                                        <h3>Patient Name</h3>
                                        <h6 class="fw-normal text-primary mb-3">Profession</h6>
                                    </div>
                                    <div class="testimonial-item text-center">
                                        <div class="position-relative mb-5">
                                            <img class="img-fluid rounded-circle mx-auto" src="img/testimonial-2.jpg" alt="">
                                            <div class="position-absolute top-100 start-50 translate-middle d-flex align-items-center justify-content-center bg-white rounded-circle" style="width: 60px; height: 60px;">
                                                <i class="fa fa-quote-left fa-2x text-primary"></i>
                                            </div>
                                        </div>
                                        <p class="fs-4 fw-normal">Dolores sed duo clita tempor justo dolor et stet lorem kasd labore dolore lorem ipsum. At lorem lorem magna ut et, nonumy et labore et tempor diam tempor erat. Erat dolor rebum sit ipsum.</p>
                                        <hr class="w-25 mx-auto">
                                        <h3>Patient Name</h3>
                                        <h6 class="fw-normal text-primary mb-3">Profession</h6>
                                    </div>
                                    <div class="testimonial-item text-center">
                                        <div class="position-relative mb-5">
                                            <img class="img-fluid rounded-circle mx-auto" src="img/testimonial-3.jpg" alt="">
                                            <div class="position-absolute top-100 start-50 translate-middle d-flex align-items-center justify-content-center bg-white rounded-circle" style="width: 60px; height: 60px;">
                                                <i class="fa fa-quote-left fa-2x text-primary"></i>
                                            </div>
                                        </div>
                                        <p class="fs-4 fw-normal">Dolores sed duo clita tempor justo dolor et stet lorem kasd labore dolore lorem ipsum. At lorem lorem magna ut et, nonumy et labore et tempor diam tempor erat. Erat dolor rebum sit ipsum.</p>
                                        <hr class="w-25 mx-auto">
                                        <h3>Patient Name</h3>
                                        <h6 class="fw-normal text-primary mb-3">Profession</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
        <!-- Feedback End -->


        <!-- Blog Start -->
        <div class="container-fluid py-5">
            <div class="container">
                <div class="text-center mx-auto mb-5" style="max-width: 500px;">
                    <h5 class="d-inline-block text-primary text-uppercase border-bottom border-5">Blog Post</h5>
                    <h1 class="display-4">Our Latest Medical Blog Posts</h1>
                </div>
                <div class="row g-5">
                    <c:forEach items="${listpl}" var="o">
                        <div class="col-xl-3 col-lg-6">
                            <div class="bg-light rounded overflow-hidden">
                                <img class="img-fluid w-100" src="${o.post_souce_url}" alt="">
                                <div class="p-4">
                                    <a class="h3 d-block mb-3" href="postdetail?pid=${o.post_id  }">${o.post_content}</a>
                                    <p class="m-0">${o.post_description}</p>
                                </div>
                                <div class="d-flex justify-content-between border-top p-4">
                                    <div class="d-flex align-items-center">
                                        <img class="rounded-circle me-2" src="img/user.jpg" width="25" height="25" alt="">
                                        <small>${o.post_created_date}</small>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <small class="ms-3"><i class="far fa-eye text-primary me-1"></i>12345</small>
                                        <small class="ms-3"><i class="far fa-comment text-primary me-1"></i>123</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>


                </div>
            </div>
        </div>
        <!-- Blog End -->



    </div>
</div>
</div>
<!-- Blog End -->

<!--         Footer Start 
<%@include file="footer.jsp" %>
 Footer End -->


<!-- Back to Top -->
<a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="lib/easing/easing.min.js"></script>
<script src="lib/waypoints/waypoints.min.js"></script>
<script src="lib/owlcarousel/owl.carousel.min.js"></script>
<script src="lib/tempusdominus/js/moment.min.js"></script>
<script src="lib/tempusdominus/js/moment-timezone.min.js"></script>
<script src="lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

<!-- Template Javascript -->
<script src="js/main.js"></script>



</body>

</html>