<%-- 
    Document   : display_invoices.jsp
    Created on : Jun 4, 2023, 5:45:46 PM
    Author     : Son Nguyen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

        <style>
            @import url('https://fonts.googleapis.com/css2?family=Fira+Sans+Extra+Condensed:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');@import url('https://fonts.googleapis.com/css2?family=Heebo:wght@100;200;300;400;500;600;700;800;900&display=swap');@import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap');:root{
                --font1: 'Heebo', sans-serif;
                --font2: 'Fira Sans Extra Condensed', sans-serif;
                --font3: 'Roboto', sans-serif
            }
            body{
                font-family: var(--font3);
                background-image: linear-gradient(120deg, #fdfbfb 0%, #ebedee 100%);

            }

            h2{
                font-weight: 900
            }

            .card-img, .card-img-top{
                border-top-left-radius: calc(1rem - 1px);
                border-top-right-radius: calc(1rem - 1px)
            }

            .card-img-top{
                width: 100%;
                max-height: 180px;
                object-fit: contain;
                padding: 30px
            }
            .card h2{
                font-size: 1rem
            }

            .label-top{
                position: absolute;
                background-color: #8bc34a;
                color: #fff;
                top: 8px;
                right: 8px;
                padding: 5px 10px 5px 10px;
                font-size: .7rem;
                font-weight: 600;
                border-radius: 3px;
                text-transform: uppercase
            }
            .top-right{
                position: absolute;
                top: 24px;
                left: 24px;
                width: 90px;
                height: 90px;
                border-radius: 50%;
                font-size: 1rem;
                font-weight: 900;
                background: #ff5722;
                line-height: 90px;
                text-align: center;
                color: white
            }
            .top-right span{
                display: inline-block;
                vertical-align: middle
            }
            @media (max-width: 768px){
                .card-img-top{
                    max-height: 250px
                }
            }
            .over-bg{
                background: rgba(53, 53, 53, 0.85);
                box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
                backdrop-filter: blur(0.0px);
                -webkit-backdrop-filter: blur(0.0px);
                border-radius: 10px
            }
            /*            .btn{
                            font-size: 1rem;
                            font-weight: 500;
                            text-transform: uppercase;
                            padding: 5px 50px 5px 50px
                        }
            */            .box .btn{
                font-size: 1.5rem
            }
            @media (max-width: 1025px){
                .btn{
                    padding: 5px 40px 5px 40px
                }
            }
            @media (max-width: 250px){
                .btn{
                    padding: 5px 30px 5px 30px
                }
            }
            .btn-warning{
                background: none #f7810a;
                color: #ffffff;
                fill: #ffffff;
                border: none;
                text-decoration: none;
                outline: 0;
                box-shadow: -1px 6px 19px rgba(247, 129, 10, 0.25);
                border-radius: 100px
            }
            .btn-warning:hover{
                background: none #ff962b;
                color: #ffffff;
                box-shadow: -1px 6px 13px rgba(255, 150, 43, 0.35)
            }
            .bg-success{
                font-size: 1rem;
                background-color: #f7810a !important
            }
            .bg-danger{
                font-size: 1rem
            }
            .price-hp{
                font-size: 1rem;
                font-weight: 600;
                color: darkgray
            }
            .amz-hp{
                font-size: .7rem;
                font-weight: 600;
                color: darkgray
            }
            .fa-question-circle:before{
                color: darkgray
            }
            .fa-plus:before{
                color: darkgray
            }
            .box{
                border-radius: 1rem;
                background: #fff;
                box-shadow: 0 6px 10px rgb(0 0 0 / 8%), 0 0 6px rgb(0 0 0 / 5%);
                transition: .3s transform cubic-bezier(.155, 1.105, .295, 1.12), .3s box-shadow, .3s -webkit-transform cubic-bezier(.155, 1.105, .295, 1.12)
            }
            .box-img{
                max-width: 300px
            }
            .thumb-sec{
                max-width: 300px
            }
            @media (max-width: 576px){
                .box-img{
                    max-width: 200px
                }
                .thumb-sec{
                    max-width: 200px
                }
            }
            .inner-gallery{
                width: 60px;
                height: 60px;
                border: 1px solid #ddd;
                border-radius: 3px;
                margin: 1px;
                display: inline-block;
                overflow: hidden;
                -o-object-fit: cover;
                object-fit: cover
            }
            @media (max-width: 370px){
                .box .btn{
                    padding: 5px 40px 5px 40px;
                    font-size: 1rem
                }
            }
            .disclaimer{
                font-size: .9rem;
                color: darkgray
            }
            .related h3{
                font-weight: 900
            }
            footer{
                background: #212529;
                height: 80px;
                color: #fff
            }

            .navbar img{
                width:25px;
                height: 25px;
                border-radius:10px;

            }
            .item_name{
                padding:10px 20px;
                border-radius: 10px;
                color: #1d2a4d;
                font-weight:600;
                background:#f3f3f3;

            }

            .item_name:hover {
                box-shadow: 10px 10px 5px #eceaea;

            }


            #search_name,#newest_invoice,#price,#appearance{
                outline: none;
                border: none;
                text-indent: 15px;
                border-radius: 20px;
                height: 30px;
                box-shadow: 10px 10px 10px rgba(148, 147, 147, 0.511);
                background: #13c5dd;
                color: white;
            }
            .placeholder::placeholder{
                color: white;
                padding-left:5px;
            }

        </style>
    </head>
    <body>



        <div class="col-md-8 ">

            <div class="container navbar mt-0 d-flex justify-content-center align-content-center col-md-12 ">
                <img class="col-md-2" src="img/search.gif" alt="Search Invoice"> 
                <input id="search_name" oninput="searchInvoices(this)" class="placeholder col-md-6 mr-2" placeholder="Search follow service name or doctor name" type="text">
                <select id="price" class="col-md-2 mr-2 placeholder" style="margin-left: 3px;" onclick="sortByPrice(this)">
                    <option value="increase">Increase</option>
                    <option value="decrease">Decrease</option>

                    <option  class="placeholder"disabled selected hidden>Total Money </option>

                </select>
                <select id="appearance" class="col-md-2 mr-2 placeholder" style="margin-left: 3px;" onclick="sortByAppearance(this)">
                    <option value="newest" selected="">Newest Invoice</option>
                    <option value="earliest">Earliest Invoice </option>


                </select>
                <!--                <button id="newest_invoice" class="col-md-2 placeholder" style="margin-left: 3px;" onclick="sortByNewestInvoices(this)">Newest Invoices</button>-->
            </div>

            <div class="container bg-trasparent pb-3 p-2  m-1 pt-0" id="invoices"> 


                <% for (Invoice i : invoices) { %>

                <div class="card h-100 shadow-sm row mb-4" style="box-shadow: 10px 10px 10px rgba(148, 147, 147, 0.3);">
                    <div class="card-header text-center m-0">
                        <a href="invoice_detail?invoice_id=<%= i.getInvoiceID() %>">
                            <span style="color: #1d2a4d; font-weight: 600; font-size: 20px;">
                                Invoice ID: <%= i.getInvoiceID() %>
                            </span>
                        </a>

                    </div>




                    <%  
         for (MedicalReport mr : i.getReports()) {   
                 Service ser = mr.getService();
                    %>

                    <div class="row col-md-11  "  style="height:70px;border-bottom:2px solid #eceaea;margin: 0px 35px ">
                        <div class="col-md-3 p-2 text-center">

                            <img src="<%= ser.getImage() %>" width="85px" height="50px" style="border-radius:10px;"  />
                        </div>
                        <div class="col-md-6 p-2 text-center" >
                            <div class="item_name ">  <h7 style="color: #1d2a4d;font-weight:600"> <%= ser.getName() %></h7> </div>
                        </div>





                        <div class="col-md-3 p-2 text-center ">
                            <p style="color: #1d2a4d;text-align:center;font-weight: 600">
                                Service <%= ser.getId() %>:
                                <span style="color: #ff6666; font-size: 20px; font-weight: 600;"><%= ser.getPrice() %> $</span>

                            </p>
                        </div>
                    </div>


                    <%        }     %>

                    <% for (int m = 0; m < i.getMedicineItems().size(); m++) { %>
                    <% MedicineItem me = i.getMedicineItems().get(m); %>
                    <div class="row col-md-11" style="height:70px;border-bottom:2px solid #eceaea;margin: 0px 35px">
                        <div class="col-md-3 p-a text-center">

                            <img src="<%=me.getMedicine().getMedicine_image()%>" width="85px" height="50px" style="border-radius:10px;"/>
                        </div>
                        <div class="col-md-6 p-2 text-center" >
                            <div class="item_name "> <h7 > <%= me.getMedicine().getMedicine_name() %></h7></div>
                        </div>



                        <div class="col-md-3 p-2 text-center ">
                            <p style="color: #1d2a4d;text-align:center;font-weight: 600;">
                                Medicine <%= me.getMedicine().getMedicine_id() %>:
                                <span style="color: #ff6666; font-size: 20px; font-weight: 600;"><%= me.getMedicine().getMedicine_price() %> $</span><span style="color:#cccccc;">(x<%=me.getMedicineQuantity()%>)</span>
                            </p>
                        </div>
                    </div>
                    <% } %>

                    <div class="col-md-12 pb-1 card-footer ">

                        <p style="color: #1d2a4d;text-align:right;font-weight: 600">
                            <img src="img/cost.png" width="40px" height="40px" alt="alt"/>TotaL Money: 
                            <span style="color: #ff6666; font-size: 25px; font-weight: 600;margin-right: 12px;"><%=i.getTotalMoney()%> $</span>
                        </p>

                    </div>

                    <div class="col-md-12 row justify-content-center">
                        <div class="col-md-3 text-center item_name m-2 p-2">
                            <a href="profile?s?evaluated_s">
                                <img style="border: solid #999999 2px; border-radius: 10px;" src="img/evalute.gif" width="40px" height="40px" alt="alt" />
                                Evaluate
                            </a>
                        </div>
                        <div class="col-md-3 text-center item_name m-2 p-2">
                            <a href="profile?s?rebook_s">
                                <img style="border: solid #999999 2px; border-radius: 10px;" src="img/booking.gif" width="40px" height="40px" alt="alt" />
                                Rebook
                            </a>
                        </div>
                        <div class="col-md-3 text-center item_name m-2 p-2">
                            <a href="mailto:soncrt1234@gmail.com">
                                <img style="border: solid #999999 2px; border-radius: 10px;" src="img/mail.gif" width="40px" height="40px" alt="alt" />
                                Send email
                            </a>
                        </div>
                    </div>

                </div>


                <% } %>




            </div>
        </a>
    </div>

    <script>
        function searchInvoices(nameInput) {
            var searchName = nameInput.value;
            console.log(searchName);
            $.ajax({
                url: "search_or_filter_invoices",
                method: "POST",
                data: {search_name: searchName},
                success: function (response) {
                    var row = document.getElementById("invoices");

                    row.innerHTML = response;


                },
                error: function (xhr, status, error) {
                    // Xử lý lỗi (nếu có)
                    console.log(error);
                }
            });

//            
        }
        function sortByPrice(priceSelect) {
            var sortPrice = priceSelect.value;
            console.log(sortPrice);

            $.ajax({
                url: "search_or_filter_invoices",
                method: "POST",
                data: {sort_price: sortPrice},
                success: function (response) {
                    var row = document.getElementById("invoices");

                    row.innerHTML = response;


                },
                error: function (xhr, status, error) {
                    // Xử lý lỗi (nếu có)
                    console.log(error);
                }
            });
        }
        function sortByAppearance(appearanceSelect) {
            var appearance = appearanceSelect.value;
            console.log(appearance);

            $.ajax({
                url: "search_or_filter_invoices",
                method: "POST",
                data: {invoice_appearance: appearance},
                success: function (response) {
                    var row = document.getElementById("invoices");

                    row.innerHTML = response;


                },
                error: function (xhr, status, error) {
                    // Xử lý lỗi (nếu có)
                    console.log(error);
                }
            });
        }

    </script>
</body>
</html>
