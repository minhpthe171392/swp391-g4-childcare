<%@page import="model.Service" %>

<html>
    <head>
        <title>title</title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

        <style>
            /*
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/css.css to edit this template
            */
            /* 
                Created on : Feb 12, 2023, 3:51:43 PM
                Author     : son nguyen
            */
            body{
                margin-top:20px;
                color: #484b51;
            }
            .text-secondary-d1 {
                color: #13c5dd!important;
            }
            .page-header {
                margin: 0 0 1rem;
                padding-bottom: 1rem;
                padding-top: .5rem;
                border-bottom: 1px dotted #e2e2e2;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-pack: justify;
                justify-content: space-between;
                -ms-flex-align: center;
                align-items: center;
            }
            .page-title {
                padding: 0;
                margin: 0;
                font-size: 1.75rem;
                font-weight: 300;
            }
            .brc-default-l1 {
                border-color: #dce9f0!important;
            }

            .ml-n1, .mx-n1 {
                margin-left: -.25rem!important;
            }
            .mr-n1, .mx-n1 {
                margin-right: -.25rem!important;
            }
            .mb-4, .my-4 {
                margin-bottom: 1.5rem!important;
            }

            hr {
                margin-top: 1rem;
                margin-bottom: 1rem;
                border: 0;
                border-top: 1px solid rgba(0,0,0,.1);
            }

            .text-grey-m2 {
                color: #1d2a4d!important;
            }

            .text-success-m2 {
                color: #1d2a4d!important;
            }

            .font-bolder, .text-600 {
                font-weight: 600!important;
            }

            .text-110 {
                font-size:#1d2a4d!important;
            }
            .text-blue {
                color: #1d2a4d!important;
            }
            .pb-25, .py-25 {
                padding-bottom: .75rem!important;
            }

            .pt-25, .py-25 {
                padding-top: .75rem!important;
            }
            .bgc-default-tp1 {
                background-color: rgba(121,169,197,.92)!important;
            }
            .bgc-default-l4, .bgc-h-default-l4:hover {
                background-color: #f3f8fa!important;
            }
            .page-header .page-tools {
                -ms-flex-item-align: end;
                align-self: flex-end;
            }

            .btn-light {
                color: #1d2a4d;
                background-color: #f5f6f9;
                border-color: #dddfe4;
            }
            .w-2 {
                width: 1rem;
            }

            .text-120 {
                font-size: 120%!important;
            }
            .text-primary-m1 {
                color: #13c5dd!important;
            }

            .text-danger-m1 {
                color: #13c5dd!important;
            }
            .text-blue-m2 {
                color: #13c5dd!important;
            }
            .text-150 {
                font-size: 150%!important;
            }
            .text-60 {
                font-size: 60%!important;
            }
            .text-grey-m1 {
                color: #13c5dd!important;
            }
            .align-bottom {
                vertical-align: bottom!important;
            }
            .page-content{
                border:1px solid #cccccc;
                border-radius:10px;
            }
            .text-container {
                display: flex;
                align-items: center;
                height: 100%;
            }

            button{
                height: 30px;
                box-shadow: 30px 30px 30px rgba(148, 147, 147, 0.911);
                border-radius:3px;
                background: #d9edf7;
                border:none;
            }
            .navbar button{
                border-radius: 15px;
            }
            .quantity-inputer{
                box-shadow: 30px 30px 30px rgba(148, 147, 147, 0.911);
                background: rgba(18, 20, 20, 0.5);
                border-radius:10px;
            }
            .navbar input{
                border-radius: 15px;
                height: 30px;
                box-shadow: 30px 30px 30px rgba(148, 147, 147, 0.911);
                border-radius:15px;
                background: rgba(18, 20, 20, 0.5);
                color: white;
            }
            .navbar input:focus{
                border:solid #d9edf7 2px;
            }
            thead tr{
                background-color:#d9edf7;
                border-radius:10px;
            }

            input[type="text"]::placeholder,input[type="text"] {
                padding-left:10px;
                color: white;

            }
            input[type="text"] {
                outline: none;
                border: none;

            }
            .navbar img{
                width:25px;
                height: 25px;
                box-shadow: 30px 30px 30px rgba(148, 147, 147, 0.911);
                border-radius:10px;

            }/*
             
            
            */
            .navbar img:hover{
                width:35px;
                height: 35px;
                box-shadow: 30px 30px 30px rgba(148, 147, 147, 0.911);
                background-color:rgba(189, 191, 191,0.2) ;
            }/*
            
            */
            .navbar{
                margin-top: 30px;
                text-align: center;

            }
            .navbar input{
                width:372px;
            }


            th{
                color:#1d2a4d;
                font-family:Noto Sans;
                font-size:17px;
                height: 30px;
                width:auto;

            }
            .table th ,.table tr{
                text-align: left;
            }


            td{
                color:#1d2a4d;
                font-family:Noto Sans;
                height: 20px;
                width: auto;

            }/*
            */
            tbody tr:hover{
                background-color:rgba(189, 191, 191,0.1) ;

            }


            tbody td:hover{
                background-color:rgba(189, 191, 191,0.1) ;
                border-bottom: #d9edf7 solid 2px;

            }/*
            */
            .table{
                /*                background: rgba(18, 20, 20, 0.1);*/
                border:20px;
            }


            button:hover{
                font-size: 17px;
            }
        </style>
    </head>
    <body>
        <div class="page-content container col-md-8 p-3 mt-5">
            <div class="page-header text-blue-d2 card-header m-0">
                <h1 class="page-title text-secondary-d1">
                    Invoice
                    <small class="page-info">
                        <i class="fa fa-angle-double-right text-80"></i>
                        ID: #<%=invoice.getInvoiceID()%>
                    </small>
                </h1>

                <div class="page-tools">
                    <div class="action-buttons">
                        <a class="btn bg-white btn-light mx-1px text-95" href="#" data-title="Print">
                            <i class="mr-1 fa fa-print text-primary-m1 text-120 w-2"></i>
                            Print
                        </a>
                        <a class="btn bg-white btn-light mx-1px text-95" href="#" data-title="PDF">
                            <i class="mr-1 fa fa-file-pdf-o text-danger-m1 text-120 w-2"></i>
                            Export
                        </a>
                    </div>
                </div>
            </div>

            <div class="container px-0">
                <div class="row mt-4">
                    <div class="col-12 col-lg-12">

                        <!-- .row -->


                        <div class="row">
                            <div class="col-sm-6">
                                <div>
                                    <span class="text-sm text-grey-m2 align-middle">To:</span>
                                    <span class="text-600 text-110 text-blue align-middle"><%=user.getName()%></span>
                                </div>
                                <div class="text-grey-m2">
                                    <div class="my-1">
                                        <%=user.getAddress()%>
                                    </div>

                                    <div class="my-1"><i class="fa fa-phone fa-flip-horizontal text-secondary"></i> <b class="text-600"><%=user.getPhone()%></b></div>
                                </div>
                            </div>
                            <!-- /.col -->

                            <div class="text-95 col-sm-6 align-self-start d-sm-flex justify-content-end">
                                <hr class="d-sm-none" />
                                <div class="text-grey-m2">


                                    <div class="my-2"><i class="fa fa-circle  text-xs mr-1"></i> <span class="text-600 text-90"> Doctor:</span> <%=invoice.getDoctorName()%></div>

                                    <div class="my-2"><i class="fa fa-circle text-xs mr-1"></i> <span class="text-600 text-90"> Nurse:</span> <%=invoice.getNurseName()%></div>


                                    <div class="my-2"><i class="fa fa-circle  text-xs mr-1"></i> <span class="text-600 text-90">Slot start time:</span> <%=invoice.getServiceStartTime()%></div>

                                    <div class="my-2"><i class="fa fa-circle  text-xs mr-1"></i> <span class="text-600 text-90">Slot end time:</span> <%=invoice.getServiceEndTime()%></div>


                                    <div class="my-2"><i class="fa fa-circle  text-xs mr-1"></i> <span class="text-600 text-90">Date and Time:</span><%=invoice.getDateAndTime()%></div>

                                </div>
                            </div>
                            <!-- /.col -->
                        </div>



                        <div class="">
                            <table class="table " >

                                <tr class="card-header ">
                                    <th class="text-uppercase">Service name</th>                       
                                    <th class="text-uppercase">Symptoms    </th>              
                                    <th class="text-uppercase">Conclusion  </th>
                                    <th class="text-uppercase">Price       </th>


                                </tr>
                                <%  for(Service s: invoice.getServices()){%>
                                <tr style="height: 150px;width: 150px" class="col-md-12">
                                    <td class="col-md-2"> <%=s.getName()%> </td>
                                    <td class="col-md-4">  <%=s.getSymptoms()%>  </td>
                                    <td class="col-md-4"><%=s.getConclusion()%> </td>
                                    <td class="col-md-2">
                                        <span style="color: #ff6666; font-size: 20px; font-weight: 600;"><%= s.getPrice() %>$ </span></td>
                                    <td>
                                </tr>
                                <%}%>
                            </table>          

                            <table class="table" >

                                <tr class="card-header ">
                                    <th class="text-uppercase">Medicine name</th>    
                                    <th class="text-uppercase">Quantity      </th>
                                    <th class="text-uppercase">Price  </th>


                                </tr>
                                <%  for(Medicine m: invoice.getMedicines()){%>
                                <tr style="height: 150px;width: 150px" class="col-md-12">
                                    <td class="col-md-4"> <%=m.getMedicine_name()%> </td>
                                    <td class="col-md-6"><%=m.getMedicine_quantity()%> </td>
                                    <td class="col-md-2" style="color: #ff6666; font-size: 20px; font-weight: 600">  <%=m.getMedicine_price()%>$ </td>

                                </tr>
                                <%}%>
                            </table>          

                        </div>


                        <div class="col-md-12 pb-1 card-footer">
                            <p style="color: #13c5dd;text-align:right;">
                                <img src="img/cost.png" width="50px" height="50px" alt="alt"/>TotaL Money: 
                                <span style="color: #ff6666; font-size: 25px; font-weight: 600;margin-right: 12px;"><%=totalMoney%> $</span>
                            </p>
                        </div>

                      
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>

