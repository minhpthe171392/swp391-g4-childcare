<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Invoice" %>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Medicine Management
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <!-- CSS Files -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../assets/css/paper-dashboard.css?v=2.0.1" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="../assets/demo/demo.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <style>
            .margin-top-50 {
                margin-top: 50px;
            }
            /*Filter START*/
            .filterable {
                margin-top: 15px;
            }
            .filterable .panel-heading .pull-right {
                margin-top: -20px;
            }
            .filterable .filters input[disabled] {
                background-color: transparent;
                border: none;
                cursor: auto;
                box-shadow: none;
                padding: 0;
                height: auto;
            }
            .filterable .filters input[disabled]::-webkit-input-placeholder {
                color: #333;
            }
            .filterable .filters input[disabled]::-moz-placeholder {
                color: #333;
            }
            .filterable .filters input[disabled]:-ms-input-placeholder {
                color: #333;
            }
            /*Filter END*/

            .table-widthB{
                width: 48%;
            }

            .table-widthA{
                width: 49.8%;
            }

            .bg{
                background-color: white;
            }

            .tablescroll {
                overflow-y: auto;
                overflow-x: hidden;
                height: 189px;
                margin-right: 1px;
            }
            .marginTop30{
                margin-top:30px;
            }

            .radio,
            .checkbox {
                margin-top: 0px;
                margin-bottom: 0px;
            }

            .checkbox,.radio{
                margin-top:0px;
                margin-bottom:0px
            }

            .radio-margin{
                margin-left: -13px;
                margin-top: 7px;
            }
            .radio-margin{
                margin-left: -13px;
                margin-top: 7px;
            }
            .EU_DataTable td, th {
                padding: 6px;
                border: 1px solid #ccc;
                text-align: left;
                height: 50px;
            }
            th {
                background: #e5e5e5;
                color: #454545;
                font-weight: bold;
                height: 40px;
            }
            /*Radio and Checkbox START*/
            .checkbox label:after,
            .radio label:after {
                content: '';
                display: table;
                clear: both;
            }

            .checkbox .cr,
            .radio .cr {
                position: relative;
                display: inline-block;
                border: 1px solid #a9a9a9;
                border-radius: .25em;
                width: 1.3em;
                height: 1.3em;
                float: left;
                margin-right: .5em;
            }

            .radio .cr {
                border-radius: 50%;
            }

            .checkbox .cr .cr-icon,
            .radio .cr .cr-icon {
                position: absolute;
                font-size: .8em;
                line-height: 0;
                top: 50%;
                left: 20%;
            }

            .checkbox label input[type="checkbox"],
            .radio label input[type="radio"] {
                display: none;
            }

            .checkbox label input[type="checkbox"] + .cr > .cr-icon,
            .radio label input[type="radio"] + .cr > .cr-icon {
                transform: scale(3) rotateZ(-20deg);
                opacity: 0;
                transition: all .3s ease-in;
            }

            .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
            .radio label input[type="radio"]:checked + .cr > .cr-icon {
                transform: scale(1) rotateZ(0deg);
                opacity: 1;
            }

            .checkbox label input[type="checkbox"]:disabled + .cr,
            .radio label input[type="radio"]:disabled + .cr {
                opacity: .5;
            }
            .table th, td{
                text-align: center;
            }

            .text-right{
                width: 230px;
            }
            .table button{
                padding: 10px 20px;
                font-size: 15px;
                background: #51cbce;
                color: white;
                border: none;
                outline: none;
                cursor: pointer;
                border-radius: 10px;
            }
            .form h2{
                text-align: center;
                color: #222;
                margin: 10px auto;
                font-size: 20px;
            }
            .form .form-element{
                text-align: left;
                margin: 15px 0px;
            }
            .popup .form .form-element label{
                font-size:14px;
                color:#222;
            }
            .form .form-element textarea, input, select{
                margin-top: 5px;
                display: block;
                width:100%;
                padding:10px;
                outline:none;
                border:1px solid #aaa;
                border-radius:5px;
            }
            .form .form-element button{
                height: 40px;
                border:none;
                outline:none;
                font-size:15px;
                background: #51cbce;
                color: #F5F5F5;
                border-radius: 10px;
                cursor: pointer;
            }
        </style>
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-color="white" data-active-color="danger">
                <div class="logo">
                    <a href="https://www.creative-tim.com" class="simple-text logo-mini">
                        <div class="logo-image-small">
                            <img src="../assets/img/logo-small.png">
                        </div>
                        <!-- <p>CT</p> -->
                    </a>
                    <a href="https://www.creative-tim.com" class="simple-text logo-normal">
                        Creative Tim
                        <!-- <div class="logo-image-big">
                          <img src="../assets/img/logo-big.png">
                        </div> -->
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li>
                            <a href="user"> 
                                <i class="nc-icon nc-single-02"></i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li class="active ">
                            <a href="doctor">
                                <i class="nc-icon nc-tile-56"></i>
                                <p>Medical Exam</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">

                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>   
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="javascript:;">Medical Exam</a>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header row">
                                    <h3 class="card-title col-md-8">Invoice Number: ${invoice.getInvoiceID()}</h3>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="card-body">
                                    <div class="form">
                                        <form action="invoiceDetail" method="post">
                                            <input type="text" name="invId" id="invId" value="${invoice.getInvoiceID()}" hidden>
                                            <div class="form-element">
                                                <label for="name">Name</label>
                                                <input type="text" name="name" id="name" value="${invoice.getCusName()}" disabled>
                                            </div>
                                            <div class="creatediv1 marginTop30">
                                                <div class="col-md-12" style="padding:0">
                                                    <div class="panel panel-primary filterable" style="border: solid #51cbce 1px;">
                                                        <div class="panel-heading" style="background-color: #51cbce; border:none;">
                                                            <h3 class="panel-title">Cost Centers<span style="color: white; font-weight: bold;"> *</span></h3>
                                                            <div class="pull-right">
                                                                <button type="button" style="margin:0" class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <tr class="filters">
                                                                <th style="width: 4.1%; width:50px;">
                                                                    <div class="checkbox radio-margin">
                                                                        <label>
                                                                            <input type="checkbox" value="">
                                                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        </label>
                                                                    </div>
                                                                </th>
                                                                <th style="width: 48%">
                                                                    <input type="text" class="form-control" placeholder="Service Name" disabled>
                                                                </th>
                                                                <th style="width: 48%">
                                                                    <input type="text" class="form-control" placeholder="Price" disabled>
                                                                </th>
                                                            </tr>
                                                        </table>
                                                        <div class="bg tablescroll">
                                                            <table class="table table-bordered table-striped">
                                                                <c:forEach items="${service}" var="ser">
                                                                    <tr>
                                                                        <td style="width: 4.5%; width:50px;">
                                                                            <div class="checkbox radio-margin">
                                                                                <label>
                                                                                    
                                                                                    <input type="checkbox" name="checkbox" value=${ser.getId()} ${check ? "checked":""}>
                                                                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 48%">${ser.getName()}</td>
                                                                        <td style="width: 46.8%">${ser.getPrice()}</td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-element">
                                                <label for="conclusion">Conclusion</label>
                                                <textarea rows="6" name="conclusion" id="conclusion"></textarea>
                                            </div>
                                            <div class="form-element">
                                                <button type="submit" name="submit">Finish</button>
                                                <button type="submit" name="submit1">Save Draft</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.filterable .btn-filter').click(function () {
                    var $panel = $(this).parents('.filterable'),
                            $filters = $panel.find('.filters input'),
                            $tbody = $panel.find('.table tbody');
                    if ($filters.prop('disabled') === true) {
                        $filters.prop('disabled', false);
                        $filters.first().focus();
                    } else {
                        $filters.val('').prop('disabled', true);
                        $tbody.find('.no-result').remove();
                        $tbody.find('tr').show();
                    }
                });

                $('.filterable .filters input').keyup(function (e) {
                    /* Ignore tab key */
                    var code = e.keyCode || e.which;
                    if (code === '9')
                        return;
                    /* Useful DOM data and selectors */
                    var $input = $(this),
                            inputContent = $input.val().toLowerCase(),
                            $panel = $input.parents('.filterable'),
                            column = $panel.find('.filters th').index($input.parents('th')),
                            $table = $panel.find('.table'),
                            $rows = $table.find('tbody tr');
                    /* Dirtiest filter function ever ;) */
                    var $filteredRows = $rows.filter(function () {
                        var value = $(this).find('td').eq(column).text().toLowerCase();
                        return value.indexOf(inputContent) === -1;
                    });
                    /* Clean previous no-result if exist */
                    $table.find('tbody .no-result').remove();
                    /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
                    $rows.show();
                    $filteredRows.hide();
                    /* Prepend no-result row if all rows are filtered */
                    if ($filteredRows.length === $rows.length) {
                        $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="' + $table.find('.filters th').length + '">No result found</td></tr>'));
                    }
                });
            });
        </script>
        <!--   Core JS Files   -->
        <script src="../assets/js/core/jquery.min.js"></script>
        <script src="../assets/js/core/popper.min.js"></script>
        <script src="../assets/js/core/bootstrap.min.js"></script>
        <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!--  Google Maps Plugin    -->
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
        <!-- Chart JS -->
        <script src="../assets/js/plugins/chartjs.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="../assets/js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../assets/js/paper-dashboard.min.js?v=2.0.1" type="text/javascript"></script><!-- Paper Dashboard DEMO methods, don't include it in your project! -->
        <script src="../assets/demo/demo.js"></script>
    </body>
</html>