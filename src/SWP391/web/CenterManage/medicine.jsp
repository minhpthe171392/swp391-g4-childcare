<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Medicine" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Medicine Management
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <!-- CSS Files -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../assets/css/paper-dashboard.css?v=2.0.1" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="../assets/demo/demo.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <style>
            .table th, td{
                text-align: center;
            }
            #popup-update-background, #popup-add-background{
                display: none;
                width:100%;
                height:100%;
                position: fixed;
                z-index: 5000;
                background-color:rgba(120, 120, 120, 0.8);
            }
            .text-right{
                width: 230px;
            }
            #table-paging{
                text-align: center;
            }

            #table-paging a{
                font-size: 15px;
                padding-left: 5px;
            }
            .table button{
                padding: 10px 20px;
                font-size: 15px;
                background: #51cbce;
                color: white;
                border: none;
                outline: none;
                cursor: pointer;
                border-radius: 10px;
            }
            .popup{
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                width:500px;
                padding: 0px 40px;
                background: #FFFFFF;
                box-shadow: 2px 2px 5px 5px rgba(0,0,0,0.15);
                border-radius: 10px;
            }
            .popup .close-btn{
                position: absolute;
                top: 10px;
                right:10px;
                width:15px;
                height:15px;
                background:#888;
                color:#eee;
                text-align: center;
                line-height: 15px;
                border-radius: 15px;
                cursor: pointer;
            }
            .popup .form h2{
                text-align: center;
                color: #222;
                margin: 10px auto;
                font-size: 20px;
            }
            .popup .form .form-element{
                text-align: left;
                margin: 15px 0px;
            }
            .popup .form .form-element label{
                font-size:14px;
                color:#222;
            }
            .popup .form .form-element input, select{
                margin-top: 5px;
                display: block;
                width:100%;
                padding:10px;
                outline:none;
                border:1px solid #aaa;
                border-radius:5px;
            }
            .popup .form .form-element button{
                width:100%;
                height: 40px;
                border:none;
                outline:none;
                font-size:15px;
                background: #51cbce;
                color: #F5F5F5;
                border-radius: 10px;
                cursor: pointer;
            }
            .card-header button{
                width:100%;
                height: 40px;
                border:none;
                outline:none;
                font-size:15px;
                background: #51cbce;
                color: #F5F5F5;
                border-radius: 10px;
                cursor: pointer;
            }

        </style>
    </head>

    <body class="">
        <div class="wrapper ">
            <div id="popup-add-background">
                <div class="popup">
                    <div class="close-btn">&times;</div>
                    <div class="form">
                        <form action="medicine" method="post">
                            <h2>Add Medicine</h2>
                            <div class="form-element">
                                <input hidden type="text" name="option" value="3">
                            </div>
                            <div class="form-element">
                                <label for="name">Name</label>
                                <input type="text" name="name" required>
                            </div>
                            <div class="form-element">
                                <label for="unit">Unit</label><br>
                                <select name="unit" required>
                                    <option value="Unit 1">Unit 1</option>
                                    <option value="Unit 2">Unit 2</option>
                                    <option value="Unit 3">Unit 3</option>
                                    <option value="Unit 4">Unit 4</option>
                                </select>
                            </div>
                            <div class="form-element">
                                <label for="quantity">Quantity</label>
                                <input type=number name="quantity" required>
                            </div>
                            <div class="form-element">
                                <label for="price">Price</label>
                                <input type=number name="price" required>
                            </div>
                            <div class="form-element">
                                <button type="submit">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div id="popup-update-background">
                <div class="popup">
                    <div class="close-btn">&times;</div>
                    <div class="form">
                        <form action="medicine" method="post">
                            <h2>Update Medicine</h2>
                            <div class="form-element">
                                <input hidden type="text" name="id" id="id">
                                <input hidden type="text" name="option" value="1">
                            </div>
                            <div class="form-element">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" required>
                            </div>
                            <div class="form-element">
                                <label for="unit">Unit</label><br>
                                <select name="unit" id="unit" required>
                                    <option value="Unit 1">Unit 1</option>
                                    <option value="Unit 2">Unit 2</option>
                                    <option value="Unit 3">Unit 3</option>
                                    <option value="Unit 4">Unit 4</option>
                                </select>
                            </div>
                            <div class="form-element">
                                <label for="quantity">Quantity</label>
                                <input type=number name="quantity" id="quantity" required>
                            </div>
                            <div class="form-element">
                                <label for="price">Price</label>
                                <input oninput="check()" type=number name="price" id="price" required>
                            </div>
                            <div class="form-element">
                                <button type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="sidebar" data-color="white" data-active-color="danger">
                <div class="logo">
                    <a href="https://www.creative-tim.com" class="simple-text logo-mini">
                        <div class="logo-image-small">
                            <img src="../assets/img/logo-small.png">
                        </div>
                        <!-- <p>CT</p> -->
                    </a>
                    <a href="https://www.creative-tim.com" class="simple-text logo-normal">
                        Creative Tim
                        <!-- <div class="logo-image-big">
                          <img src="../assets/img/logo-big.png">
                        </div> -->
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li>
                            <a href="user">
                                <i class="nc-icon nc-single-02"></i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="medicine">
                                <i class="nc-icon nc-tile-56"></i>
                                <p>Manage Medicine</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">

                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="javascript:;">Manage Medicine</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end" id="navigation">
                            <form action="search" methos="post">
                                <div class="input-group no-border">
                                    <input type="text" oninput="searchByName(this)" value="${txt}" class="form-control">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <i class="nc-icon nc-zoom-split"></i>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header row">
                                    <h3 class="card-title col-md-8"> Medicine List</h3>
                                    <div class="col-md-2"></div>
                                    <!--                                    <select class="card-title col-md-2" name="filter-option" style="padding-right:10px">
                                                                            <option></option>
                                                                            <option></option>
                                                                            <option></option>
                                                                        </select>-->
                                    <button class="card-title col-md-2" onclick="showAdd()">Add</button>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive" id="medicine-table">
                                        <table class="table">
                                            <thead class=" text-primary">
                                            <th hidden></th>
                                            <th style="text-align: left">Name</th>
                                            <th>Quantity</th>
                                            <th>Unit</th>
                                            <th>Price(VND)</th>
                                            <th></th>
                                            </thead>
                                            <tbody id="table-row">
                                                <c:forEach items="${listM}" var="med">
                                                    <tr>
                                                <form action="medicine" method="post">
                                                    <td hidden>${med.getId()}</td>
                                                    <td style="text-align: left">${med.getName()}</td>
                                                    <td>${med.getQuantity()}</td>
                                                    <td>${med.getUnit()}</td>
                                                    <td>${med.getPrice()}</td>
                                                    <td class="text-right">
                                                        <button type="button" class="show-update">Update</button>
                                                        <input hidden type="text" name="option" value="2">
                                                        <input hidden type="text" name="id" value="${med.getId()}">
                                                        <button type="submit">Disable</button>
                                                    </td>
                                                </form>
                                                </tr>
                                            </c:forEach>
                                            <tr id="table-paging">
                                                <td colspan="5">
                                                    <c:forEach begin="1" end="${endPage}" var="i">
                                                        <a class="${tag==i?"active":""}" id="page-number" href="medicine?index=${i}">${i}</a>
                                                    </c:forEach>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer footer-black  footer-white ">
                    <div class="container-fluid">
                        <div class="row">
                            <nav class="footer-nav">
                                <ul>
                                    <li><a href="https://www.creative-tim.com" target="_blank">Creative Tim</a></li>
                                    <li><a href="https://www.creative-tim.com/blog" target="_blank">Blog</a></li>
                                    <li><a href="https://www.creative-tim.com/license" target="_blank">Licenses</a></li>
                                </ul>
                            </nav>
                            <div class="credits ml-auto">
                                <span class="copyright">
                                    <script>
                                        document.write(new Date().getFullYear());
                                    </script>, made with <i class="fa fa-heart heart"></i> by Creative Tim
                                </span>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                // code to read selected table row cell data (values).
                $(".show-update").on('click', function () {
                    var currentRow = $(this).closest("tr");
                    var id = currentRow.find("td:eq(0)").text();
                    var name = currentRow.find("td:eq(1)").text();
                    var quantity = currentRow.find("td:eq(2)").text();
                    var unit = currentRow.find("td:eq(3)").text();
                    var price = currentRow.find("td:eq(4)").text();
                    document.getElementById("id").value = id;
                    document.getElementById("name").value = name;
                    document.getElementById("quantity").value = quantity;
                    document.getElementById("unit").value = unit;
                    document.getElementById("price").value = price;
                    document.getElementById("popup-update-background").style.display = 'block';
                });
            });
            function searchByName(param) {
                var txtSearch = param.value;
                $.ajax({
                    url: "searchAjax",
                    type: "get",
                    data: {
                        txt: txtSearch
                    },
                    success: function (data) {
                        var row = document.getElementById("table-row");
                        row.innerHTML = data;
                    },
                    error: function (xhr) {
                        alert("Can't search");
                    }
                });
            }
            ;
            function showUpdate() {
                document.getElementById("popup-update-background").style.display = 'block';
            }
            ;
            function showAdd() {
                document.getElementById("popup-add-background").style.display = 'block';
            }
            ;
            const nodeList = document.querySelectorAll(".popup .close-btn");
            for (let i = 0; i < nodeList.length; i++) {
                nodeList[i].addEventListener("click", function hide() {
                    document.getElementById("popup-add-background").style.display = 'none';
                    document.getElementById("popup-update-background").style.display = 'none';
                });
            }
        </script>
        <!--   Core JS Files   -->
        <script src="../assets/js/core/jquery.min.js"></script>
        <script src="../assets/js/core/popper.min.js"></script>
        <script src="../assets/js/core/bootstrap.min.js"></script>
        <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!--  Google Maps Plugin    -->
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
        <!-- Chart JS -->
        <script src="../assets/js/plugins/chartjs.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="../assets/js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../assets/js/paper-dashboard.min.js?v=2.0.1" type="text/javascript"></script><!-- Paper Dashboard DEMO methods, don't include it in your project! -->
        <script src="../assets/demo/demo.js"></script>
    </body>

</html>