<%-- 
    Document   : answers
    Created on : Jul 10, 2023, 1:19:58 AM
    Author     : LT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="css/styles.css" rel="stylesheet" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Thư viện jQuery -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

        <!-- Thư viện Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

        <!-- Thư viện Bootstrap JS -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    </head>
    <head>
        <meta charset="utf-8">
        <title>MEDINOVA - Post</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free HTML Templates" name="keywords">
        <meta content="Free HTML Templates" name="description">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&family=Roboto:wght@400;700&display=swap" rel="stylesheet">  

        <!-- Icon Font Stylesheet -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Libraries Stylesheet -->
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

        <!-- Customized Bootstrap Stylesheet -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
        <style>
            .question {
                margin-bottom: 20px;
                padding: 10px;
                background-color: #f2f2f2;
                border: 1px solid #ccc;
                border-radius: 5px;
            }


            .question-title {
                font-size: 18px;
                font-weight: bold;
                color: #333;
            }


            .see-answers-btn {
                padding: 5px 10px;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 5px;
                cursor: pointer;
                transition: background-color 0.3s ease;
                width: 125px;
            }

            .see-answers-btn:hover {
                background-color: #0056b3;
            }


        </style>

    </head>
    <body>
        <%User user=(User)session.getAttribute("user");%>
        <%@include file="navigate.jsp" %>
        <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <meta name="description" content="" />
            <meta name="author" content="" />
            <title>Blog Post - Start Bootstrap Template</title>
            <!-- Favicon-->
            <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
            <!-- Core theme CSS (includes Bootstrap)-->
            <link href="css/styles.css" rel="stylesheet" />
        </head>
        <body>
            <!-- Page content-->
            <div>
                <div class="container mt-5">
                    <div class="row">
                        <!--                            start category-->

                        <div class="col-lg-2 category" style='height:100%;padding: 0;border: solid 1px #ccc; border-radius: 5px;background: #eff5f9'>
                            <div class="card-header text-left" style='font-size:20px'><b>Category</b></div>
                            <ul class="category-list"  style='list-style-type: none; padding-left:0;margin-bottom:0'>
                                <c:forEach items="${listcc}" var="o">
                                    <li class="card-body" style='border:solid 1px #ccc'>
                                        <a href="categorydetail?cid=${o.category_id}" class="category-link" style='font-size:20px; text-align: left; color: #1a1a1a'><b>${o.getCategory_name()}</b></a>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>

                        <!--                            end category-->
                        <div class="col-lg-7">
                            <!-- Post content-->

                            <div class="question">
                                <div class="question-date card-header"  style="font-style: italic;">date ask: ${questionn.getQuestion_date()}</div>
                                <img src="https://scontent.fhan12-1.fna.fbcdn.net/v/t1.18169-9/24058951_893563900825779_3593682861528713749_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=WURBBcvyBroAX_Zo-5W&_nc_ht=scontent.fhan12-1.fna&oh=00_AfDkFVQTxcaHlYOmgTh6U8BUWkxwWCYGWgWGvjjYvbD9mQ&oe=64CF813F" alt="Avatar" class="avatar" style="width: 50px;
                                     height: 50px;">
                                <span class="question-name">${questionn.getQuestion_name()}</span>

                                <p class="question-emai">${questionn.getQuestion_email()}</p>

                                <p class="h3 d-block mb-3">${questionn.getQuestion_title()}?</p>

                                <p class="m-0">${questionn.getQuestion_description()}</p>

                                <div class="d-flex align-items-center mt-3 card-footer ">
                                    <small class="ms-3"><i class="far fa-eye text-primary me-1"></i>12345</small>
                                    <small class="ms-3"><i class="far fa-comment text-primary me-1"></i>123</small>

                                </div>

                                <!--                                    start  answers-->
                                <c:forEach items="${answers}" var="o">
                                    <div class="d-flex">
                                        <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                        <div class="ms-3">

                                            <div class="fw-bold">${o.answers_name} </div>
                                            <div>${o.answers_description}</div>
                                            <div class="text-muted">Date Created: ${o.created_at}</div>

                                        </div>
                                    </div>                
                                </c:forEach>
                                <!--                                          end answers-->
                                <!--                                        cmt-->

                                <form class="mb-4" action="answers" method="post">
                                    <div>
                                        <input type="text" name="question_id" value="${questionn.id}" hidden >
                                        <input class="form-control" type="text" name="name" placeholder="Your Name" required>
                                        <textarea class="form-control" rows="3" name="comment" placeholder="Join the discussion and leave a comment!" required></textarea>
                                        <input type="submit" value="Send">
                                    </div>
                                </form>

                                <!--                                        end cmt-->
                            </div>


                        </div>
                        <div class="col-lg-3">
                            <!--nút question-->

                            <form action="">
                                <div class="input-group">
                                    <a href="#addEmployeeModal"  class="btn btn-info" data-toggle="modal"><span>Add New Question</span></a>
                                </div>
                            </form>

                            <!-- Kết thúc nút -->

                            <!--top question new-->
                            <div class="card bg-light mb-3">
                                <div class="card-header text-center text-dark" >Top Recent Question</div>

                                <c:forEach items="${listt}" var="o" varStatus="loop">        
                                    <ul class="category-list" >

                                        <a href="#" class="category-link"  >
                                            <h5 class="question-title" >${loop.index + 1}.${o.question_title}</h5>
                                        </a>

                                    </ul>
                                </c:forEach>
                            </div>
                            <!--end top-->

                        </div>



                    </div>
                </div>
            </div>
        </div>
        <div id="addEmployeeModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="question" method="post">
                        <div class="modal-header">						
                            <h4 class="modal-title">Add Question</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">					
                            <div class="form-group">
                                <label>Name</label>
                                <input name="name" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input name="mail" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <textarea name="title" class="form-control" required></textarea>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control" required></textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-success" value="Add">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
</html>
<html>
</body>
</html>
<!-- template end -->
<!-- Footer Start -->
<%@include file="footer.jsp" %>
<!-- Footer End -->

<!-- Back to Top -->
<a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>

</html>
