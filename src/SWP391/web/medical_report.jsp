
<html>
    <head>
        <title>title</title>
        <style>
            /*
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/css.css to edit this template
            */
            /* 
                Created on : Feb 12, 2023, 3:51:43 PM
                Author     : son nguyen
            */
            body{
                color: #484b51;
            }
            .text-secondary-d1 {
                color: #13c5dd!important;
            }
            .page-header {
                margin: 0 0 1rem;
                padding-bottom: 1rem;
                padding-top: .5rem;
                border-bottom: 1px dotted #e2e2e2;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-pack: justify;
                justify-content: space-between;
                -ms-flex-align: center;
                align-items: center;
            }
            .page-title {
                padding: 0;
                margin: 0;
                font-size: 1.75rem;
                font-weight: 300;
            }
            .brc-default-l1 {
                border-color: #dce9f0!important;
            }

            .ml-n1, .mx-n1 {
                margin-left: -.25rem!important;
            }
            .mr-n1, .mx-n1 {
                margin-right: -.25rem!important;
            }
            .mb-4, .my-4 {
                margin-bottom: 1.5rem!important;
            }

            hr {
                margin-top: 1rem;
                margin-bottom: 1rem;
                border: 0;
                border-top: 1px solid rgba(0,0,0,.1);
            }

            .text-grey-m2 {
                color: #1d2a4d!important;
            }

            .text-success-m2 {
                color: #1d2a4d!important;
            }

            .font-bolder, .text-600 {
                font-weight: 600!important;
            }

            .text-110 {
                font-size:#1d2a4d!important;
            }
            .text-blue {
                color: #1d2a4d!important;
            }
            .pb-25, .py-25 {
                padding-bottom: .75rem!important;
            }

            .pt-25, .py-25 {
                padding-top: .75rem!important;
            }
            .bgc-default-tp1 {
                background-color: rgba(121,169,197,.92)!important;
            }
            .bgc-default-l4, .bgc-h-default-l4:hover {
                background-color: #f3f8fa!important;
            }
            .page-header .page-tools {
                -ms-flex-item-align: end;
                align-self: flex-end;
            }

            .btn-light {
                color: #1d2a4d;
                background-color: #f5f6f9;
                border-color: #dddfe4;
            }
            .w-2 {
                width: 1rem;
            }

            .text-120 {
                font-size: 120%!important;
            }
            .text-primary-m1 {
                color: #13c5dd!important;
            }

            .text-danger-m1 {
                color: #13c5dd!important;
            }
            .text-blue-m2 {
                color: #13c5dd!important;
            }
            .text-150 {
                font-size: 150%!important;
            }
            .text-60 {
                font-size: 60%!important;
            }
            .text-grey-m1 {
                color: #13c5dd!important;
            }
            .align-bottom {
                vertical-align: bottom!important;
            }
            .page-content{
                border:1px solid #cccccc;
                border-radius:10px;
            }
            .text-container {
                display: flex;
                align-items: center;
                height: 100%;
            }



            tbody tr:hover, tbody td:hover{
                background-color:#f3f3f3;

            }


           td,th{
                border-right:solid 1px #cdcdcd;

            }
            .table{
                color: #1d2a4d;
                border:20px;
            }



        </style>
    </head>
    <body>

        <div class="col-md-8 pl-3" >
            <div class="page-content container mt-5 p-3" style="box-shadow: 10px 10px 10px rgba(148, 147, 147, 0.3)">
                <div class="page-header card-header m-0 text-container" style="display: flex; align-items: center;">
                    <h6 class="page-title" style="font-size: 20px; color: #1d2a4d; font-weight: 600;">
                        <img src="img/report.png" alt="alt" style="width: 35px; height: 35px; ">

                        Medical Report
                        <small class="page-info">
                            <i class="fa fa-angle-double-right text-80"></i>
                        </small>
                    </h6>
                </div>


                <div class="container px-0">
                    <div class="row mt-4">
                        <div class="col-12 col-lg-12">

                            <!-- .row -->


                            <div class="col-sm-6 m-3 ">


                                <div class="text-grey-m2">
                                    <div class="my-2"><i class="fa fa-circle  text-xs mr-1"></i> 
                                        <span class="text-600 text-90">Customer:</span>
                                        <%=user.getName()%>
                                    </div>
                                    <div class="my-1">
                                        <i class="fa fa-circle  text-xs mr-1"></i>
                                        <span class="text-600 text-90">Address:</span>

                                        <%=user.getAddress()%>
                                    </div>

                                    <div class="my-1"><i class="fa fa-circle  text-xs mr-1"></i>
                                        <span class="text-600 text-90">Phone:</span>

                                        <%=user.getPhone()%>
                                    </div>

                                    <div class="my-1"><i class="fa fa-circle  text-xs mr-1"></i>
                                        <span class="text-600 text-90">Email:</span>

                                        <%=user.getEmail()%>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->


                            <!-- /.col -->

                            <div class="">
                                <table class="table  table-responsive table-borderless">
                                    <tr class="card-header col-md-12 text-capitalize" style="color: #1d2a4d;border-bottom:solid 1px #cdcdcd">
                                        <th class=" col-md-2 " >Examination date</th>    
                                        <th class=" col-md-10 ">Conclusion    </th>
                                    </tr>
                                    <%  for(MedicalReport mr: reports){%>
                                    <tr class="col-md-12" style="color: #1d2a4d;border-bottom:solid 1px #cdcdcd" >
                                        <td class=" col-md-2 "><%=mr.getDateAndTime()  %></td>

                                        <td class="col-md-10"><%=mr.getConclusion()%> </td>

                                    </tr>
                                    <%}%>
                                </table>          



                            </div>





                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

