<%-- 
    Document   : user_edit
    Created on : Jun 2, 2023, 10:39:30 PM
    Author     : Son Nguyen
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Profile</title>
    </head>
    <body>

        <div class="col-md-8 pt-5" style="padding:0px 12px">
            <div class="card card-user" style="box-shadow: 10px 10px 10px rgba(148, 147, 147, 0.3);">
                <div class="card-header  ">
                    <h5 class="card-title text-center">Edit Profile <img  src="img/edit.png" width="25px" height="25px" alt="alt"/></h5>
                </div>
                <div class="card-body">
                    <form action="profile" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="service" value="update_profile"  >
                        <input type="hidden" name="cus_id" value="<%=user.getId()%>"  >
                        <div class="row col-md-12">                                        
                            <div class="col-md-6 pr-1 form-group">
                                <label>
                                    <h6>
                                        Email
                                    </h6>

                                </label>
                                <input name ="cus_email" id="emailInput" type="text" class="form-control" readonly placeholder=" email" value="<%=user.getEmail()%>">
                            </div>
                            <div class="col-md-6 px-1 form-group ">
                                <label><h6>Name</h6></label>
                                <input name="cus_name" type="text" class="form-control" placeholder="Enter full name" value="<%=user.getName()%>">
                            </div>
                        </div>
                        <div class="row col-md-12">                                        
                            <div class="col-md-6 pr-1 form-group">
                                <label>
                                    <h6>Password
                                        <span style="color: #13c5dd;font-weight:300;">
                                            <a class="password_change.jsp" href="#">(change)</a>
                                        </span>
                                    </h6>

                                </label>
                                <input name="cus_password" type="password" class="form-control" disabled="" placeholder="" value="<%=user.getPassword()%>" readonly>
                            </div>
                            <div class="col-md-6 px-1 form-group">
                                <label>
                                    <h6>Phone
                                        <span style="color: #13c5dd;font-weight:300;">
                                        </span>
                                    </h6>

                                </label>           
                                <input name="cus_phone" id="phoneInput" type="phone" class="form-control" placeholder="" value="<%=user.getPhone()%>" readonly>
                            </div>
                        </div>
                        <div class="row col-md-12">                                        
                            <div class="form-group col-md-6 pr-1">
                                <label><h6>Date of birth</h6></label>
                                <input name="cus_dob"  type="date" class="form-control   " placeholder="Enter date of birth" value="<%=user.getDob()%>">
                            </div>  
                            <div class="col-md-6 px-1 form-group">
                                <label><h6>Gender</h6></label>
                                <select name="cus_gender" id="labels mb-0" class="form-control"  >
                                    <option value="male" <%=user.getGender().equalsIgnoreCase("Male") ? "selected" : "" %>>Male</option>
                                    <option value="female" <%=user.getGender().equalsIgnoreCase("Female") ? "selected" : "" %>>Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="row col-md-12">                                        
                            <div class="form-group col-md-6 pr-1">
                                <label><h6>Address</h6></label>
                                <input name="cus_address" type="text" class="form-control" placeholder="Enter address" value="<%=user.getAddress()%>">
                            </div>
                            <div class="form-group col-md-6 px-1">
                                <label><h6>Chosen picture</h6></label>
                                <input name="cus_photo" type="file" class="form-control " placeholder="Open file" >
                            </div>                                         
                        </div>
                        <div class="row ">                                        
                            <div class="update ml-auto mr-auto col-md-6 text-center mt-3">
                                <button type="submit" class="btn btn-primary btn-round">Update Profile
                                </button>
                            </div>
                            <div class=" reset ml-auto mr-auto col-md-6 text-center mt-3">
                                <button class="btn btn-primary btn-round" type="reset">Reset
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
