/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.service.ServiceListDAO;
import dao.category.CategoryDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import model.Category;
import model.Service;

/**
 *
 * @author Son Nguyen
 */
@WebServlet(name = "GetServiceController", urlPatterns = {"/get_service"})
public class GetServiceController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            List<Service> services;
            ServiceListDAO serviceDAO = new ServiceListDAO();
            CategoryDAO cateDAO = new CategoryDAO();
            String categoryID = request.getParameter("category_id");
            System.out.println(categoryID);
            Category cate = cateDAO.getCategoryByID(categoryID);
            services = ((categoryID != null && !categoryID.isEmpty()) ? serviceDAO.getServicesByCategory(categoryID) : serviceDAO.getServiceByLimit(""));
            System.out.println(services);
            for (Service s : services) {
                out.print("<div class=\"service col-xl-3 col-lg-6\" >\n"
                        + "                            <div class=\"bg-light rounded overflow-hidden\">\n"
                        + "                                <img class=\"img-fluid w-100\" src=\"" + s.getImage() + "\" style=\"width: 400px;height:224px;object-fit:cover\">\n"
                        + "                                <div class=\"p-4\" style=\"max-height:150px;\">\n"
                        + "                                    <a class=\"h3 d-block mb-3\" href=\"postdetail?pid=${s.id }\">" + s.getName() + "</a>\n"
                        + "                                    <div class=\"col-md-12 row\">\n"
                        + "                                        <div class=\"col-md-9\" >\n"
                        + "                                            <p style=\"color: #1d2a4d;text-align:left;font-weight: 600\">\n"
                        + "                                                Service ID(" + s.getId() + "):\n"
                        + "                                                <span style=\"color: #ff6666; font-size: 20px; font-weight: 600;\">" + s.getPrice() + " $</span>\n"
                        + "                                            </p>\n"
                        + "                                        </div>\n"
                       
                        + "                                    </div>"
                        + "                                </div>\n"
                        + "                                  <div class=\"d-flex justify-content-between border-top p-4  col-md-12\">\n"
                        + "                                        <h5 style=\"color:#1d2a4d; \">Reservation  address </h5><br>\n"
                        + "                                        <span><p> 123 Ton That Thuyet,Ha Noi</p></span>\n"
                        + "\n"
                        + "                              \n"
                        + "                                </div>"
                        + "                            </div>\n"
                        + "                        </div>");
            }
            request.setAttribute(categoryID, out);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
