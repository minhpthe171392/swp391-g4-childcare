/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.medicineDAO.MedicineDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import model.Medicine;

/**
 *
 * @author Admin
 */
@WebServlet(name = "MedicineController", urlPatterns = {"/CenterManage/medicine"})
public class MedicineController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String indexPage = request.getParameter("index");
        if (request.getParameter("index") == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);
        String txtSearch = request.getParameter("txt");
        if(request.getParameter("txt")==null)
            txtSearch = "";
        System.out.println(txtSearch);
        MedicineDAO dao = new MedicineDAO();
        int count = dao.getTotalAccount(txtSearch);
        System.out.println(count);
        int endPage = count / 5;
        if (count % 5 != 0) {
            endPage++;
        }
        List<Medicine> list = dao.SearchByName(txtSearch, index);
        request.setAttribute("listM", list);
        request.setAttribute("endPage", endPage);
        request.setAttribute("tag", index);
        request.getRequestDispatcher("medicine.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String option = request.getParameter("option");
        String medID = request.getParameter("id");
        String name = "";
        String unit = "";
        String errorMess = "";
        int quantity, price;
        MedicineDAO dao = new MedicineDAO();
        switch (option) {
            case "1":
                try {
                name += request.getParameter("name");
                unit += request.getParameter("unit");
                quantity = Integer.parseInt(request.getParameter("quantity"));
                price = Integer.parseInt(request.getParameter("price"));
                if (price % 1000 != 0) {
                    errorMess = "Price should be rounded to 1000VND";
                } else {
                    Medicine med = new Medicine(medID, name, unit, quantity, price);
                    dao.updateMedicine(med);
                }
            } catch (Exception e) {
                System.out.println("Option 1:" + e.getMessage());
            }
            break;
            case "2":
                try {
                Medicine med = new Medicine(medID);
                dao.deleteMedicine(med);
            } catch (Exception e) {
                System.out.println("Option 2:" + e.getMessage());
            }
            break;
            case "3":
                try {
                name += request.getParameter("name");
                unit += request.getParameter("unit");
                quantity = Integer.parseInt(request.getParameter("quantity"));
                price = Integer.parseInt(request.getParameter("price"));
                if (price % 1000 != 0) {
                    errorMess = "Price should be rounded to 1000VND";
                } else {
                    Medicine med = new Medicine(name, unit, quantity, price);
                    dao.addMedicine(med);
                    System.out.println("case 3 finish");
                }
            } catch (Exception e) {
                System.out.println("Option 3:" + e.getMessage());
            }
            break;
            default:
                System.out.println("No input option");
                break;
        }
        String indexPage = request.getParameter("index");
        if (request.getParameter("index") == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);
        String txtSearch = request.getParameter("txt");
        if(request.getParameter("txt")==null)
            txtSearch = "";
        System.out.println(txtSearch);
        int count = dao.getTotalAccount(txtSearch);
        System.out.println(count);
        int endPage = count / 5;
        if (count % 5 != 0) {
            endPage++;
        }
        List<Medicine> list = dao.SearchByName(txtSearch, index);
        request.setAttribute("listM", list);
        request.setAttribute("endPage", endPage);
        request.setAttribute("tag", index);
        request.getRequestDispatcher("medicine.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
