/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.commonDAO.SearchDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import model.Medicine;

/**
 *
 * @author Admin
 */
public class SearchAJAX extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        
        String indexPage = request.getParameter("index");
        if (request.getParameter("index") == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);
        String txtSearch = request.getParameter("txt");
        SearchDAO dao = new SearchDAO();
        int count = dao.getTotalAccount(txtSearch);
        int endPage = count / 5;
        if (count % 5 != 0) {
            endPage++;
        }
        List<Medicine> list = dao.SearchByName(txtSearch, index);
        PrintWriter out = response.getWriter();
        for (Medicine med : list) {
            out.println(
                    "<tr>"
                    + "<form action=\"medicine\" method=\"post\">"
                    + "     <td style=\"text-align: left\">"+med.getName()+"</td>"
                    + "     <td>"+med.getQuantity()+"</td>"
                    + "     <td>"+med.getUnit()+"</td>"
                    + "     <td>"+med.getPrice()+"vnd</td>"
                    + "     <td class=\"text-right\">"
                    + "         <button type=\"button\" id=\"show-update\" value=\""+med.getId()+"\">"
                    + "                 Update</button>"
                    + "         <input hidden name=\"option\" value=\"2\">"
                    + "         <input hidden name=\"id\" value=\""+med.getId()+"\">"
                    + "         <button type=\"submit\">Delete</button>"
                    + "     </td>"
                    + "     </form>"
                    + "</tr>"
            );
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
