/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.invoice.InvoiceListDAO;
import dao.user.ProfileDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.InvoiceList;
import model.User;

/**
 *
 * @author Son Nguyen
 */
@WebServlet(name = "ProfileDetailController", urlPatterns = {"/profile_detail"})
public class ProfileDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            ProfileDAO profileDAO = new ProfileDAO();
            InvoiceListDAO invoiceDAO=new InvoiceListDAO();
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("user");
            String userID = user.getId();
            InvoiceList invoices =new InvoiceList(invoiceDAO.getInvoicesByCusID(userID));
            String userRank = profileDAO.getRankByCustomerId(userID);
            String totalMoney = invoices.getTotalMoneyByInvoices();
            session.setAttribute("invoice_number", invoices.getInvoices().size());
            session.setAttribute("total_money", totalMoney);
            session.setAttribute("user_rank", userRank);
            request.setAttribute("invoices", invoices.getInvoices());
            request.setAttribute("service", "edit_profile");
            request.getRequestDispatcher("user_index.jsp").forward(request, response);

        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
