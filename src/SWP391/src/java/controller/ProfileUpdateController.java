/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.user.ProfileDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.User;

/**
 *
 * @author Son Nguyen
 */
@WebServlet(name = "ProfileUpdateController", urlPatterns = {"/profile_update"})
public class ProfileUpdateController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            ProfileDAO dao = new ProfileDAO();
            String cusID = request.getParameter("cus_id");
            String cusName = request.getParameter("cus_name");
            String cusEmail = request.getParameter("cus_email");
            String cusPhone = request.getParameter("cus_phone");
            String cusAddress = request.getParameter("cus_address");
            String cusPassword = request.getParameter("cus_password");
            String cusDob = request.getParameter("cus_dob");
            String cusGender = request.getParameter("cus_gender");
            String filePart = request.getParameter("cus_photo");
            try {
//                    String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
//                    InputStream fileContent = filePart.getInputStream();

                // Save file to a location on the server
//                        String uploadDirectory = "/SWP391/uploads";
//                        String filePath = request.getServletContext().getRealPath(uploadDirectory + File.separator + fileName);
                dao.updateProfile(new User(cusID, cusEmail, cusPassword, cusName, cusPhone, cusGender, cusDob, cusAddress, filePart));
                request.setAttribute("service", "");
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
             
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
