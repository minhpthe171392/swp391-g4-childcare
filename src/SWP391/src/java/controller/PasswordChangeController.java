/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.user.PasswordChangeDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.User;

/**
 *
 * @author LT
 */
@WebServlet(name="PasswordChangeController", urlPatterns={"/password_change"})
public class PasswordChangeController extends HttpServlet {

    private static final String PASSWORD_PATTERN
            = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])"
            + "(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$";

    private static final Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

    public static boolean isValid(final String password) {
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("password_change.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean check = false;
        String mess = "";
        String userID = request.getParameter("id");
        String inputPass = request.getParameter("inputPass");
        String newPass = request.getParameter("newPass");
        String rePass = request.getParameter("rePass");
        User u = new User("1");
        PasswordChangeDAO DAO = new PasswordChangeDAO();
        if (request.getParameter("inputPass").isEmpty()||
                request.getParameter("newPass").isEmpty()||
                request.getParameter("rePass").isEmpty()) {
            mess = "You should fill in all field.";
        } else if (!DAO.PasswordVerification(u, inputPass)) {
            mess = "Current password does not match.";
        } else if (!isValid(newPass)) {
            mess = "Password is not valid.";
        } else if (!rePass.equals(newPass)) {
            mess = "Re-password must be similar to new password.";
        } else {
            mess = "Password changed successfully!";
            check = true;
        }
        if(check){
            DAO.updatePassword(u, newPass);
        }
        request.setAttribute("mess", mess);
        request.getRequestDispatcher("password_change.jsp").forward(request, response);
    }

}
