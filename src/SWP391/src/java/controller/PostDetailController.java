/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.post.PostCommentDAO;
import dao.post.PostDetailDAO;
import dao.category.CategoryDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Category;
import model.Comments;
import model.Post;

/**
 *
 * @author LT
 */
@WebServlet(name = "PostDetailController", urlPatterns = {"/HealthCare/postdetail"})
public class PostDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
           
            

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("pid");
        
        PostCommentDAO daocmt = new PostCommentDAO();
        PostDetailDAO dao = new PostDetailDAO();
        CategoryDAO daoo = new CategoryDAO();
        
        List<Comments> list = daocmt.getComment();
        request.setAttribute("list", list);
        
        Post postdetail = dao.getDetaiPostlByPostId(id);
        request.setAttribute("detail", postdetail);
        
        List<Category> listc = daoo.getAllCategory();
        request.setAttribute("listcc", listc );
          
        
        request.getRequestDispatcher("HealthCare/post_detail.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("pid");
        String comment = request.getParameter("comment");
        
        
         PostCommentDAO dao = new PostCommentDAO();
         dao.insertComment(comment);
        
        PostCommentDAO daocmt = new PostCommentDAO();
        List<Comments> list = daocmt.getComment();
        request.setAttribute("list", list);
        
        CategoryDAO daoo = new CategoryDAO();
        List<Category> listc = daoo.getAllCategory();
        request.setAttribute("listcc", listc );
        
        
        PostDetailDAO daooo = new PostDetailDAO(); 
        Post postdetail = daooo.getDetaiPostlByPostId(id);
        request.setAttribute("detail", postdetail);
       
        request.getRequestDispatcher("HealthCare/post_detail.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
