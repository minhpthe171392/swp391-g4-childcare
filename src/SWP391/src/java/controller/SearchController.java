/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.search.SearchDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Service;
import model.User;

/**
 *
 * @author LT
 */
@WebServlet(name = "SearchController", urlPatterns = {"/HealthCare/search"})
public class SearchController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String txtSearch = request.getParameter("txtSearch");
            
//            if (request.getParameter("txtSearch").isEmpty())
//                txtSearch = "";
            String indexString = request.getParameter("index");
//            if (request.getParameter("index").isEmpty())
//                indexString = "1";
            int index = Integer.parseInt(indexString);
                        
            String option = request.getParameter("option");
//            if (request.getParameter("option").isEmpty())
//                option = "0";
            System.out.println(option);

            String type = "Doctor";
//            String type = request.getParameter("search-type");
//            if (request.getParameter("search-type").isEmpty())
//                type = "";
            System.out.println(type);

            SearchDAO dao = new SearchDAO();
            int count = dao.count(txtSearch);
            System.out.println(count);
//            System.out.println(type);
            System.out.println(txtSearch);
            System.out.println(index);
            System.out.println(option);
            int endPage;
            endPage = count / 2;
            if (count % 2 != 0) {
                endPage++;
            }
            switch(type){
                case "":
                    break;
                case "Doctor":
                    List<User> listDoctor = dao.SearchDoctor(txtSearch, index, option);
                    System.out.println(listDoctor);
                    request.setAttribute("list", listDoctor);
                    break;
                case "Service":
                    List<Service> listService = dao.SearchService(txtSearch, index, option);
                    System.out.println(listService);
                    request.setAttribute("list", listService);
                    break;
            }
            request.setAttribute("end", endPage);
            request.setAttribute("txtSearch", txtSearch);
            request.setAttribute("option", option);
            request.getRequestDispatcher("HealthCare/search.jsp").forward(request, response);
        } catch (Exception e) {
            System.out.println("Search: " + e.getMessage());
        }
      
//        DoctorProfileDAO dao = new DoctorProfileDAO();
//        List<User> listd = dao.getAllDoctor();
//        System.out.println(listd);
//        request.setAttribute("listdt", listd );
//        request.getRequestDispatcher("HealthCare/search.jsp").forward(request, response);      
        request.getRequestDispatcher("HealthCare/search.jsp").forward(request, response); 
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String txtSearch = request.getParameter("txtSearch");
            
            if (request.getParameter("txtSearch").isEmpty())
                txtSearch = "";
            String indexString = request.getParameter("index");
            if (request.getParameter("index").isEmpty())
                indexString = "1";
            int index = Integer.parseInt(indexString);
                        
            String option = request.getParameter("option");
            if (request.getParameter("option").isEmpty())
                option = "0";

            String type = request.getParameter("search-type");
            if (request.getParameter("search-type").isEmpty())
                type = "0";

            SearchDAO dao = new SearchDAO();
            int count = dao.count(txtSearch);
            
            int endPage;
            endPage = count / 2;
            if (count % 2 != 0) {
                endPage++;
            }
            switch(type){
                case "0":
                    break;
                case "1":
                    List<User> listDoctor = dao.SearchDoctor(txtSearch, index, option);
                    System.out.println(listDoctor);
                    request.setAttribute("list", listDoctor);
                    break;
                case "2":
                    List<Service> listService = dao.SearchService(txtSearch, index, option);
                    System.out.println(listService);
                    request.setAttribute("list", listService);
                    break;
            }
            request.setAttribute("end", endPage);
            request.setAttribute("txtSearch", txtSearch);
            request.setAttribute("option", option);
            request.getRequestDispatcher("HealthCare/search.jsp").forward(request, response);
        } catch (Exception e) {
            System.out.println("Search: " + e.getMessage());
        }
      

         
        
        
        
        request.getRequestDispatcher("HealthCare/search.jsp").forward(request, response); 
    }
        
        

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */

