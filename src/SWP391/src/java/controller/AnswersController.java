/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.question.AnswersDAO;
import dao.question.QuestionListDAO;
import dao.category.CategoryDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;
import model.Answers;
import model.Category;
import model.Question;

import model.Answers;
import model.Question;

/**
 *
 * @author LT
 */
@WebServlet(name = "AnswersController", urlPatterns = {"/HealthCare/answers"})
public class AnswersController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AnswersController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AnswersController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        AnswersDAO dao = new AnswersDAO();
        Question question = dao.getQuestionById(id);
        request.setAttribute("questionn", question);
        
        AnswersDAO daooo = new AnswersDAO();
        List<Answers> answers = daooo.getAnswersById(id);
        request.setAttribute("answers", answers);
        
        CategoryDAO daoooo = new CategoryDAO();
        List<Category> listc = daoooo.getAllCategory();
        request.setAttribute("listcc", listc);
        
        QuestionListDAO daoo = new QuestionListDAO();
        List<Question> listt = daoo.displayTopQuestion();
        request.setAttribute("listt", listt);
        System.out.println(listt);
        
        request.getRequestDispatcher("HealthCare/answers.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        AnswersDAO dao = new AnswersDAO();
        Question question = dao.getQuestionById(id);
        request.setAttribute("question", question);
        
        String idd = request.getParameter("question_id");
        String name = request.getParameter("name");
        String comment = request.getParameter("comment");
        dao.insertComment(idd, name, comment);
        
        response.sendRedirect("HealthCare/answers?id=" + idd);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
