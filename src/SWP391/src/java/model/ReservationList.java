/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import java.util.List;

/**
 *
 * @author Son Nguyen
 */
public class ReservationList {

    private List<Reservation> reservations;

    public ReservationList(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public ReservationList() {
        reservations = new ArrayList<>();
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public boolean addReservation(Reservation re) {
        return reservations.add(re);
    }

    public static class DateAndTimeCompare2 implements Comparator<Reservation> {

        @Override
        public int compare(Reservation r1, Reservation r2) {
            return r2.getDateAndTime().compareTo(r1.getDateAndTime());
        }
    }

    public List<Reservation> sortNewestReservations() {
        DateAndTimeCompare2 compare = new DateAndTimeCompare2();
        Collections.sort(reservations, compare);
        return reservations;
    }
}
