/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author LT
 */
public class Answers {
    private int id;
    private int question_id;
    private String answers_name;
    private String answers_description;
    private Date created_at;

    public Answers() {
    }

    public Answers(int id, int question_id, String answers_name, String answers_description, Date created_at) {
        this.id = id;
        this.question_id = question_id;
        this.answers_name = answers_name;
        this.answers_description = answers_description;
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public String getAnswers_name() {
        return answers_name;
    }

    public void setAnswers_name(String answers_name) {
        this.answers_name = answers_name;
    }

    public String getAnswers_description() {
        return answers_description;
    }

    public void setAnswers_description(String answers_description) {
        this.answers_description = answers_description;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "Answers{" + "id=" + id + ", question_id=" + question_id + ", answers_name=" + answers_name + ", answers_description=" + answers_description + ", created_at=" + created_at + '}';
    }
    
    
}
