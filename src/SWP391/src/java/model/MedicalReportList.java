/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Son Nguyen
 */
public class MedicalReportList {
    private  List<MedicalReport> reports;

    public MedicalReportList(List<MedicalReport> reports) {
        this.reports = reports;
    }

    public MedicalReportList() {
       reports= new ArrayList<>();
    }
    
    public List<MedicalReport> getReports() {
        return reports;
    }

    public void setReports(List<MedicalReport> reports) {
        this.reports = reports;
    }

    public  boolean addReport(MedicalReport mr){
          return  reports.add(mr);
    }
    
     public List<MedicalReport> sortByNewestDateAndTime() {
        Comparator<MedicalReport> compare = new NewestDateAndTimeCompare() ;
        Collections.sort(reports, compare);
        return reports;
    }

    public class NewestDateAndTimeCompare implements Comparator<MedicalReport> {

        @Override
        public int compare(MedicalReport mr1, MedicalReport mr2) {
            return mr2.getDateAndTime().compareTo(mr1.getDateAndTime());
        }
    }
}
