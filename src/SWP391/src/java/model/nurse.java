/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Asus
 */
public class nurse {

    public static nurse getNurse_Name(String phamdai) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private int nurse_ID;
    private String nurse_Name;
    private String password;
    private String detail;
    private String abode;

    public nurse() {
    }

    public nurse(int nurse_ID, String nurse_Name, String password) {
        this.nurse_ID = nurse_ID;
        this.nurse_Name = nurse_Name;
        this.password = password;
    }

    public int getNurse_ID() {
        return nurse_ID;
    }

    public void setNurse_ID(int nurse_ID) {
        this.nurse_ID = nurse_ID;
    }

    public String getNurse_Name() {
        return nurse_Name;
    }

    public void setNurse_Name(String nurse_Name) {
        this.nurse_Name = nurse_Name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "nurse{" + "nurse_ID=" + nurse_ID + ", nurse_Name=" + nurse_Name + ", password=" + password + '}';
    }

    public nurse(String nurse_Name, String password) {
        this.nurse_Name = nurse_Name;
        this.password = password;
    }

    public nurse(int nurse_ID, String nurse_Name, String password, String detail, String abode) {
        this.nurse_ID = nurse_ID;
        this.nurse_Name = nurse_Name;
        this.password = password;
        this.detail = detail;
        this.abode = abode;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAbode() {
        return abode;
    }

    public void setAbode(String abode) {
        this.abode = abode;
    }

   

}
