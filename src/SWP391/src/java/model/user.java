/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

//import com.google.gson.annotations.SerializedName;

import com.google.gson.annotations.SerializedName;


/**
 *
 * @author Admin
 */
public class User {

    private String id;

    private String email;

    private String password;

    private String name;

    private String phone;

    private String gender;

    private String dob;

    private String address;

    private String role;

    @SerializedName("picture")
    private String avatarUrl;

    private String accountType;

    public User(String id, String email, String password, String name, String phone, String gender, String dob, String address, String role, String avatarUrl, String accountType) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.phone = phone;
        this.gender = gender;
        this.dob = dob;
        this.address = address;
        this.role = role;
        this.avatarUrl = avatarUrl;
        this.accountType = accountType;
    }

    public User(String id, String email, String password, String name, String phone, String gender, String dob, String address, String role, String accountType) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.phone = phone;
        this.gender = gender;
        this.dob = dob;
        this.address = address;
        this.role = role;
        this.accountType = accountType;
    }

     public User(String id, String address, String avatarUrl) {
        this.id = id;
        this.address = address;
        this.avatarUrl = avatarUrl;
    }
    
     public User(String id, String email, String name,  String avatarUrl ) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.avatarUrl = avatarUrl;
    }

    public User() {
    }

    public User(String cusID, String cusEmail, String cusPassword, String cusName, String cusPhone, String cusGender, String cusDob, String cusAddress, String realPath) {
         this.id = cusID;
        this.email = cusEmail;
        this.password = cusPassword;
        this.name = cusName;
        this.phone = cusPhone;
        this.gender = cusGender;
        this.dob = cusDob;
        this.address = cusAddress;
        this.avatarUrl = realPath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(String id) {
        this.id = id;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

   

   

   
      
    /*
    author: SonNH
    date:
    description: check username (email), password in login site
     */
    public boolean checkUser() {
        try {
            String strSelect = "";
            while (true) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("checkUser: " + e.getMessage());
        }
        return false;
    }

    /*
    author: TienNV
    date:
    description: get User information by given id
     */
    public User getUserByID() {
        User data = new User();
        try {
            String strSelect = "";
            while (true) {

            }
        } catch (Exception e) {
            System.out.println("getUserByID: " + e.getMessage());
        }
        return data;
    }

    /*
    author: DaiPT
    date:
    description: add User to database 
     */
    public void addUser() {

    }

    /*
    author: SonNH
    date:
    description: get User's ID by signed-in account
     */
    public String getIDByAccount() {
        try {
            String strSelect = "";
            while (true) {
                String ID = "";
                return ID;
            }
        } catch (Exception e) {
            System.out.println("getIDByAccount: " + e.getMessage());
        }
        return "";
    }

    /*
    author: MinhPT
    date: 19/5/2023
    description: get User's password by ID
     */
    public String getPasswordByID(String inputID) {
        try {
            String strSelect = "select * from 'user' where 'user_id' = ?";
            while (true) {
                String password = "";
                return password;
            }
        } catch (Exception e) {
            System.out.println("getPasswordByID: " + e.getMessage());
        }
        return "";
    }

    /*
    author: MinhPT
    date: 19/5/2023
    description: update new password to database
     */
    public static void updatePassword() {
        try {
            String strUpdate = "update 'user' set 'user_password' = ? where 'user_id' = ?";
        } catch (Exception e) {
            System.out.println("updatePassword: " + e.getMessage());
        }
    }

    @Override
    public String toString() {
        return id + "\t" + email + "\t" + name + "\t" + phone + "\t" + gender + "\t" + dob + "\t" + address + "\t" + avatarUrl+"\t"+accountType; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

}
