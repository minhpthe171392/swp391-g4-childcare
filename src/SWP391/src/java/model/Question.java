/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author LT
 */
public class Question {
    private int id;
    private Date question_date;
    private String question_name;
    private String question_email;
    private String question_title;
    private String question_description;

    public Question() {
    }

    public Question(int id, Date question_date, String question_name, String question_email, String question_title, String question_description) {
        this.id = id;
        this.question_date = question_date;
        this.question_name = question_name;
        this.question_email = question_email;
        this.question_title = question_title;
        this.question_description = question_description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getQuestion_date() {
        return question_date;
    }

    public void setQuestion_date(Date question_date) {
        this.question_date = question_date;
    }

    public String getQuestion_name() {
        return question_name;
    }

    public void setQuestion_name(String question_name) {
        this.question_name = question_name;
    }

    public String getQuestion_email() {
        return question_email;
    }

    public void setQuestion_email(String question_email) {
        this.question_email = question_email;
    }

    public String getQuestion_title() {
        return question_title;
    }

    public void setQuestion_title(String question_title) {
        this.question_title = question_title;
    }

    public String getQuestion_description() {
        return question_description;
    }

    public void setQuestion_description(String question_description) {
        this.question_description = question_description;
    }

    @Override
    public String toString() {
        return "Question{" + "id=" + id + ", question_date=" + question_date + ", question_name=" + question_name + ", question_email=" + question_email + ", question_title=" + question_title + ", question_description=" + question_description + '}';
    }

  

   

   
    
    
    
}
