/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Son Nguyen
 */
public class InvoiceList {

    private List<Invoice> invoices;

    public InvoiceList() {
        this.invoices = new ArrayList<>();
    }

    public InvoiceList(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public List<Invoice> addInvoice(Invoice i) {
        invoices.add(i);
        return invoices;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public class IncreaseTotalMoneySort implements Comparator<Invoice> {

        @Override
        public int compare(Invoice i1, Invoice i2) {
            return Double.compare(i1.getTotalMoney(), i2.getTotalMoney());
        }
    }

    public class DecreaseTotalMoneySort implements Comparator<Invoice> {

        @Override
        public int compare(Invoice i1, Invoice i2) {
            return Double.compare(i2.getTotalMoney(), i1.getTotalMoney());
        }
    }

    public class DateAndTimeCompare implements Comparator<Invoice> {

        private String appearance;

        public DateAndTimeCompare(String appearance) {
            this.appearance = appearance;
        }

        @Override
        public int compare(Invoice i1, Invoice i2) {
            return appearance.equals("newest") ? i2.getDateAndTime().compareTo(i1.getDateAndTime()) : i1.getDateAndTime().compareTo(i2.getDateAndTime());
        }
    }

    public List<Invoice> sortByTotalMoney(String sortPrice) {
        if (sortPrice.equals("increase")) {
            Comparator<Invoice> compare = new IncreaseTotalMoneySort();
            Collections.sort(invoices, compare);

        } else {
            Comparator<Invoice> compare = new DecreaseTotalMoneySort();
            Collections.sort(invoices, compare);
        }

        return invoices;
    }

    public List<Invoice> sortByInvoiceAppearance(String appearance) {
        Comparator<Invoice> compare = new DateAndTimeCompare(appearance);
        Collections.sort(invoices, compare);
        return invoices;
    }

    public  String getTotalMoneyByInvoices() {
        double totalMoney = 0;
        for (Invoice i : invoices) {
           totalMoney+=(i.getTotalMoney());

        }

        return String.valueOf(totalMoney);
    }

    @Override
    public String toString() {
        return invoices.toString(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
}
