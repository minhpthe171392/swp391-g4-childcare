/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.user.ProfileDAO;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Son Nguyen
 */
public class Invoice {

    private String invoiceID;
    private String nurseID;
    private String nurseName;
    private String nurseEmail;
    List<MedicalReport> reports;
    List<MedicineItem> medicineItems;
    private String customerName;
    private String cusID;
    private String cusName;
    private String doctorID;
    private String doctorName;
    private String serviceStartTime;
    private String serviceEndTime;
    private Date DateAndTime;
    private Double totalMoney;

    public Invoice() {
    }

    public Invoice(String invoiceID, String nurseID, String nurseName, String nurseEmail, List<MedicalReport> reports, List<MedicineItem> medicineItems, String cusID, String cusName, String doctorID, String doctorName, String serviceStartTime, String serviceEndTime, Date DateAndTime, Double totalMoney) {
        this.invoiceID = invoiceID;
        this.nurseID = nurseID;
        this.nurseName = nurseName;
        this.nurseEmail = nurseEmail;
        this.reports = reports;
        this.medicineItems = medicineItems;
        this.cusID = cusID;
        this.cusName = cusName;
        this.doctorID = doctorID;
        this.doctorName = doctorName;
        this.serviceStartTime = serviceStartTime;
        this.serviceEndTime = serviceEndTime;
        this.DateAndTime = DateAndTime;
        this.totalMoney = totalMoney;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney() {
        double totalMoney = 0;

        for (MedicalReport r : reports) {
            totalMoney += (Double.parseDouble(r.getService().getPrice()));

        }
        for (MedicineItem m : medicineItems) {
            totalMoney += (m.getMedicine().getPrice() * m.getMedicineQuantity());
        }

        this.totalMoney = totalMoney;
    }

    public Invoice(String invoiceID, String customerName) {
        this.invoiceID = invoiceID;
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    
    public String getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(String invoiceID) {
        this.invoiceID = invoiceID;
    }

    public String getNurseID() {
        return nurseID;
    }

    public void setNurseID(String nurseID) {
        this.nurseID = nurseID;
    }

    public String getNurseName() {
        return nurseName;
    }

    public void setNurseName(String nurseName) {
        this.nurseName = nurseName;
    }

    public String getNurseEmail() {
        return nurseEmail;
    }

    public void setNurseEmail(String nurseEmail) {
        this.nurseEmail = nurseEmail;
    }

    public List<MedicineItem> getMedicineItems() {
        return medicineItems;
    }

    public void setMedicineItems(List<MedicineItem> medicineItems) {
        this.medicineItems = medicineItems;
    }

    public String getCusID() {
        return cusID;
    }

    public void setCusID(String cusID) {
        this.cusID = cusID;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(String doctorID) {
        this.doctorID = doctorID;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getServiceStartTime() {
        return serviceStartTime;
    }

    public void setServiceStartTime(String serviceStartTime) {
        this.serviceStartTime = serviceStartTime;
    }

    public String getServiceEndTime() {
        return serviceEndTime;
    }

    public void setServiceEndTime(String serviceEndTime) {
        this.serviceEndTime = serviceEndTime;
    }

    public Date getDateAndTime() {
        return DateAndTime;
    }

    public void setDateAndTime(Date DateAndTime) {
        this.DateAndTime = DateAndTime;
    }

   

    public static Invoice getInvoiceByListAndId(String id, List<Invoice> ls) {
        for (int i = 0; i < ls.size(); i++) {
            if (ls.get(i).getInvoiceID().equals(id)) {
                return ls.get(i);
            }
        }
        return null;
    }

    public void setReports(List<MedicalReport> reports) {
        this.reports = reports;
    }

    public List<MedicalReport> getReports() {
        return reports;
    }

    @Override

    public String toString() {
        return "Invoice{"
                + "invoiceID='" + invoiceID + "\n"
                + ", nurseID='" + nurseID + "\n"
                + ", nurseName='" + nurseName + "\n"
                + ", nurseEmail='" + nurseEmail + "\n"
                + ", reports=" + reports + "\n"
                + ", medicalItems=" + medicineItems + "\n"
                + ", cusID='" + cusID + "\n"
                + ", cusName='" + cusName + "\n"
                + ", doctorID='" + doctorID + "\n"
                + ", doctorName='" + doctorName + "\n"
                + ", serviceStartTime='" + serviceStartTime + "\n"
                + ", serviceEndTime='" + serviceEndTime + "\n"
                + ", DateAndTime='" + DateAndTime + "\n"
                + ", Total money='" + totalMoney + "\n"
                + '}' + "\n";
    }

}
