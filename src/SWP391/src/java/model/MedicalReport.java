/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Son Nguyen
 */
public class MedicalReport {

    private String medicalReportID;
    private String symptoms;
    private String conclusion;
    private Service service;
    private Date dateAndTime;

    public MedicalReport(String medicalReportID, String symptoms, String conclusion, Service service) {
        this.medicalReportID = medicalReportID;
        this.symptoms = symptoms;
        this.conclusion = conclusion;
        this.service = service;
    }

    public MedicalReport(String medicalReportID, String symptoms, String conclusion, Date dataAndTime) {
        this.medicalReportID = medicalReportID;
        this.symptoms = symptoms;
        this.conclusion = conclusion;
        this.dateAndTime = dataAndTime;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public String getMedicalReportID() {
        return medicalReportID;
    }

    public void setMedicalReportID(String medicalReportID) {
        this.medicalReportID = medicalReportID;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    @Override
    public String toString() {
        return  service.getName()+"\t"+ service.getPrice()+"\t"+ symptoms +"\t"+ conclusion + "\n";
    }

   

}
