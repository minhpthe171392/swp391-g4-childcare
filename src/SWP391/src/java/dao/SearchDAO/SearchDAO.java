/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.SearchDAO;

import com.mysql.cj.protocol.Message;
import dao.userDAO.DoctorProfileDAO;
import dao.categoryDAO.CategoryDAO;
import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import model.Category;
import model.Service;
import model.User;

/**
 *
 * @author LT
 */
public class SearchDAO extends dbcontext.DBContext {

    Connection conn;
    PreparedStatement ps;
    Statement stm;
    ResultSet rs;
//    public List<User> searchByName(String user_name, String role) {
//        List<User> list = new ArrayList<>();
//        String query = "select * from childcare.user where user_name like '%"
//                + user_name + "%' and user_role =" + role;
//        try {
//            conn = new DBContext().connection;
//            stm = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, 
//                    ResultSet.CONCUR_UPDATABLE);
//            rs = stm.executeQuery(query);
//            while (rs.next()) {
//                list.add(new User(rs.getString(1), rs.getString(2), rs.getString(3),
//                        rs.getString(4), rs.getString(5), rs.getString(6),
//                        rs.getString(7), rs.getString(8), rs.getString(9),
//                        rs.getString(10)));
//            }
//        } catch (Exception e) {
//            System.out.println("searchByName: "+ e.getMessage());
//        }
//       
//        return list;
//    }


    
    public int count(String user_name){
         String query = "SELECT count(*) FROM childcare.user where user_name like ?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1,"%" + user_name + "%");
            rs = ps.executeQuery();
             while(rs.next()) {                
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println("count:" + e.getMessage());
        }
        return 0;
    }

    public static void main(String[] args) {
    }

    public List<User> SearchDoctor(String txtSearch, int index, String option) {
        List<User> list = new ArrayList<>();
        try {
            String query = "select user_id, user_email, user_name, user_avatar_url "
                    + "from user where user_name like ? and user_role = 3 ";
            switch(option){
                case "0":
                    break;
                case "1":
                    query += "order by user_name asc ";
                    break;
                case "2":
                    query += "order by user_name desc ";
                    break;
            }
            query += "limit ?, ?";
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1,"%"+txtSearch+"%");
            ps.setInt(2,(index-1)*2);
            ps.setInt(3, 2);
            System.out.println(query);
            rs = ps.executeQuery();
            while(rs.next()){
               String id = rs.getString(1);
               String email = rs.getString(2);
               String name = rs.getString(3);
               String avatarUrl = rs.getString(4);
               
               list.add(new User(id, email, name, avatarUrl));
           }
        } catch (Exception e) {
             e.printStackTrace();
        }
          return list;
    }
    
    
    public List<Service> SearchService (String txtSearch, int index, String option) {
        List<Service> list = new ArrayList<>();
        try {
            String query = "select  "
                    + "from category where category_name like ? ";
            switch(option){
                case "0":
                    break;
                case "1":
                    query += "order by category_name asc ";
                    break;
                case "2":
                    query += "order by category_name desc ";
                    break;
            }
            query += "limit ?, ?";
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1,"%"+txtSearch+"%");
            ps.setInt(2,(index-1)*2);
            ps.setInt(3, 2);
            System.out.println(query);
            rs = ps.executeQuery();
            while(rs.next()){
               String id = rs.getString(1);
               String name = rs.getString(2);
               String category = rs.getString(3);
               int price = rs.getInt(4);
              list.add(new Service());
           }
        } catch (Exception e) {
             e.printStackTrace();
        }
          return list;
    }
}
      

    


