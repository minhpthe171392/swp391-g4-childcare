/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.question;

import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Answers;
import model.Question;

/**
 *
 * @author LT
 */
public class AnswersDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public Question getQuestionById(String id) {

        String query = "SELECT * FROM childcare.question where id = ?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Question(
                        rs.getInt(1),
                        rs.getDate(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    public List<Answers> getAnswersById(String id) {
        List<Answers> list = new ArrayList<>();
        String query = "SELECT * FROM childcare.answers where question_id = ?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Answers(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDate(5)));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return list;
    }
    
    
     public void insertComment(String idd, String name, String comment )
           {
        String query = "INSERT INTO answers (question_id,answer_name,answer_description) VALUES (?,?,?)";
        try {
          
            conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
             ps.setString(1, idd);
             ps.setString(2, name);
             ps.setString(3, comment);
             ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
    String idd = "1";
    String name = "hieu";
    String comment = "aaaa";
    AnswersDAO yourObject = new AnswersDAO();

    
    yourObject.insertComment(idd,name,comment);

    
   
}
}
