/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.service;

import dao.user.ProfileDAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Service;

/**
 *
 * @author Admin
 */
public class ServiceListDAO extends dbcontext.DBContext {

    public List<Service> getNextServiceThree(int amount) {
        List<Service> services = new ArrayList<>();
        try {
            PreparedStatement subqueryPre = connection.prepareStatement(
                    "SELECT MAX(service_id) AS max_id FROM service");
            ResultSet subqueryRs = subqueryPre.executeQuery();
            int maxId = 0;
            if (subqueryRs.next()) {
                maxId = subqueryRs.getInt("max_id");
            }

            if (amount == maxId) {
                return services;
            }
            PreparedStatement pre = connection.prepareStatement("SELECT *\n"
                    + "FROM service\n"
                    + "ORDER BY service_id\n"
                    + "LIMIT ?, 4;");
            pre.setInt(1, amount);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                services.add(new Service(rs.getString(1), rs.getString(2), rs.getString(4), rs.getString(6),
                        rs.getString(5), rs.getString("service_time")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceListDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return services;
    }

    public List<Service> getServiceByLimit(String limit) {
        List<Service> services = new ArrayList<>();
        try {
            String query = "SELECT * FROM service";
            if (!limit.isEmpty()) {
                query += " LIMIT ?";
            }

            PreparedStatement pre = connection.prepareStatement(query);
            if (!limit.isEmpty()) {
                pre.setInt(1, Integer.parseInt(limit));
            }

            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                services.add(new Service(
                        rs.getString(1), rs.getString(2), rs.getString(4),
                        rs.getString(6), rs.getString(5), rs.getString("service_time")
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceListDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return services;
    }

    public List<Service> getServicesByCategory(String categoryID) {
        List<Service> services = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement("SELECT * FROM service  WHERE category_id = ? ");
            pre.setString(1, categoryID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                services.add(new Service(rs.getString(1), rs.getString(2), rs.getString(4), rs.getString(6),
                        rs.getString(5), rs.getString("service_time")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceListDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return services;
    }

    public List<Service> getServicesByReservationID(String reservationID) {
        List<Service> services = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement(" select s.service_id,s.service_name,s.service_price,s.service_image,s.service_time\n"
                    + " from service_reservation as sr inner join service as s\n"
                    + " on s.service_id = sr.service_id where sr.reservation_id= ? ");
            pre.setString(1, reservationID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                services.add(new Service(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return services;
    }

    public static void main(String[] args) {
        ServiceListDAO d = new ServiceListDAO();
        System.out.println(d.getServiceByLimit(""));

    }
}
