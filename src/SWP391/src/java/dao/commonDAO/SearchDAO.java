/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.commonDAO;

import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Medicine;

/**
 *
 * @author Admin
 */
public class SearchDAO {

    Connection cnn;
    PreparedStatement pstm;
    ResultSet rs;

    public int getTotalAccount(String txtSearch) {
        try {
            String strSelect = "select count(medicine_id) from medicine"
                    + "where medicine_name like ?";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strSelect);
            pstm.setString(1, "%" + txtSearch + "%");
            rs = pstm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println("getTotalAccount: " + e.getMessage());
        }
        return 0;
    }
    
    public List<Medicine> SearchByName(String txtSearch, int index) {
        List<Medicine> medList = new ArrayList<>();
        try {
            String strSelect = "select medicine_id, medicine_name, "
                    + "medicine_unit, medicine_quantity, medicine_price "
                    + "from medicine where medicine_name like ?"
                    + "order by ? limit ?,? ";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strSelect);
            pstm.setString(1, "%" + txtSearch + "%");
            pstm.setString(2, "medicine_id");
            pstm.setInt(3, (index-1)*5);
            pstm.setInt(4, 5);
            System.out.println(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                medList.add(new Medicine(rs.getString(1), rs.getString(2),
                        rs.getString(3), Integer.parseInt(rs.getString(4)),
                        Integer.parseInt(rs.getString(5))));
            }
            System.out.println(medList);
        } catch (Exception e) {
            System.out.println("SearchByName: " + e.getMessage());
        }
        return medList;
    }

}
