/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.userDAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Invoice;
import model.InvoiceList;
import model.MedicalItem;
import model.MedicalReport;
import model.MedicalReportList;
import model.Medicine;
import model.Reservation;
import model.ReservationList;
import model.Service;
import model.User;

/**
 *
 * @author Son Nguyen
 */
public class ProfileDAO extends dbcontext.DBContext {

    public int updateProfile(User user) throws SQLException {
        int n = 0;
        PreparedStatement pre = connection.prepareStatement("UPDATE `childcare`.`user`\n"
                + "SET\n"
                + "`user_email` = ?,\n"
                + "`user_password` = ?,\n"
                + "`user_name` = ?,\n"
                + "`user_phone` = ?,\n"
                + "`user_gender` = ?,\n"
                + "`user_dob` = ?,\n"
                + "`user_address` = ?,\n"
                + "`user_avatar_url` = ?,\n"
                + "WHERE `user_id` = ?;");
        pre.setString(1, user.getEmail());
        pre.setString(2, user.getPassword());
        pre.setString(3, user.getName());
        pre.setString(4, user.getPhone());
        pre.setString(5, user.getGender());
        pre.setString(6, user.getDob());
        pre.setString(7, user.getAddress());
        pre.setString(8, user.getAvatarUrl());
        pre.setString(9, user.getId());

        n = pre.executeUpdate();
        return n;
    }

    public String getRankByCustomerId(String cusID) {
        String rank = "Tiem nang";
        try {
            PreparedStatement pre = connection.prepareStatement("SELECT `rank` FROM `customer` WHERE `customer_user_id` = ?");
            pre.setString(1, cusID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                rank = rs.getString("rank");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rank;
    }

    public List<MedicalItem> getMedicalItemsByCusIDAndInvoiceID(String cusID, String invoiceID) {
        List<MedicalItem> medicalItems = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement("SELECT i.invoice_id,"
                    + " m.medicine_id, m.medicine_name, m.medicine_price, m.medicine_quantity, m.medicine_image,"
                    + " mi.medicine_quantity as medicine_item_quantity "
                    + "FROM invoice AS i "
                    + "INNER JOIN medical_invoice AS mi ON i.invoice_id = mi.invoice_id "
                    + "INNER JOIN medicine AS m ON mi.medicine_id = m.medicine_id "
                    + "WHERE i.invoice_customer_id = ? AND i.invoice_id = ?");
            pre.setString(1, cusID);
            pre.setString(2, invoiceID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                medicalItems.add(new MedicalItem(invoiceID,
                        new Medicine(rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getInt(5), rs.getString(6)),
                        rs.getInt(7)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return medicalItems;
    }

    public List<MedicalReport> getMedicalReportsByCusIDAndInvoiceID(String cusID, String invoiceID) {
        List<MedicalReport> medicalReports = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement(" SELECT mr.medical_report_id, mr.symptoms, mr.conclusion,\n"
                    + "                    s.service_id, s.service_name, s.service_price, s.service_image,s.service_time FROM invoice AS i\n"
                    + "                    INNER JOIN service_invoice AS si ON i.invoice_id = si.invoice_id\n"
                    + "                    INNER JOIN service AS s ON si.service_id = s.service_id \n"
                    + "                    inner join medical_report as mr on mr.invoice_id=i.invoice_id and mr.service_id=s.service_id\n"
                    + "                   WHERE i.invoice_customer_id = ? and i.invoice_id= ? ;");
            pre.setString(1, cusID);
            pre.setString(2, invoiceID);

            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                medicalReports.add(new MedicalReport(rs.getString(1), rs.getString(2), rs.getString(3),
                        new Service(rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8))));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return medicalReports;
    }

    public List<Invoice> filterInvoicesByCusIDAndSortPrice(String cusID, String sortPrice) {
        InvoiceList invoices = new InvoiceList(getInvoicesByCusID(cusID));
        return invoices.sortByTotalMoney(sortPrice);
    }

    public List<Invoice> filterInvoicesByCusIDAndAppearance(String cusID, String appearance) {
        InvoiceList invoices = new InvoiceList(getInvoicesByCusID(cusID));
        return invoices.sortByInvoiceAppearance(appearance);
    }

    public List<Invoice> searchInvoicesByCusIDAndServiceNameOrDoctorName(String cusID, String searchName) {
        InvoiceList invoices = new InvoiceList(getInvoicesByCusID(cusID));
        InvoiceList invoicesSearchByServiceNameOrDoctorName = new InvoiceList();
        for (Invoice i : invoices.getInvoices()) {
            if (i.getDoctorName().contains(searchName)) {
                invoicesSearchByServiceNameOrDoctorName.addInvoice(i);
            }
            for (MedicalReport mr : i.getReports()) {
                if (mr.getService().getName().contains(searchName)) {
                    invoicesSearchByServiceNameOrDoctorName.addInvoice(i);
                }
            }
        }
        return invoicesSearchByServiceNameOrDoctorName.getInvoices();
    }

    public List<Reservation> searchReservationsByCusIDAndServiceNameOrDoctorName(String cusID, String searchName) {
        ReservationList reservations = new ReservationList(getReservationsByCusID(cusID));
        ReservationList reservationsSearchByServiceNameOrDoctorName = new ReservationList();
        for (Reservation r : reservations.getReservations()) {
            if (r.getDocName().contains(searchName)) {
                reservationsSearchByServiceNameOrDoctorName.addReservation(r);
            }
            for (Service s : r.getServices()) {
                if (s.getName().contains(searchName)) {
                    reservationsSearchByServiceNameOrDoctorName.addReservation(r);
                }
            }
        }
        return reservationsSearchByServiceNameOrDoctorName.getReservations();
    }

    public List<Reservation> getReservationaByCusIDAndStatus(String cusID, String status) {
        ReservationList reservations = new ReservationList(getReservationsByCusID(cusID));
        ReservationList reservationsSearchByStatus = new ReservationList();
        if (status.equals("All")) {
            return reservations.getReservations();
        }
        for (Reservation r : reservations.getReservations()) {
            if (r.getStatus().equals(status)) {
                reservationsSearchByStatus.addReservation(r);
            }
        }
        return reservationsSearchByStatus.getReservations();

    }

    public Invoice searchInvoiceByCusIDAndInvoiceID(String cusID, String invoiceID) {
        for (Invoice i : getInvoicesByCusID(cusID)) {
            if (i.getInvoiceID().equals(invoiceID)) {
                return i;
            }
        }
        return null;
    }

    public List<MedicalReport> getMedicalReportListByCusID(String cusID) {
        Hashtable<String, MedicalReport> reports = new Hashtable<>();
        MedicalReportList medicalReports = new MedicalReportList();
        try {
            PreparedStatement pre = connection.prepareStatement(" SELECT mr.medical_report_id, mr.symptoms, mr.conclusion,\n"
                    + "                    s.service_id,i.date_and_time FROM invoice AS i\n"
                    + "                    INNER JOIN service_invoice AS si ON i.invoice_id = si.invoice_id\n"
                    + "                    INNER JOIN service AS s ON si.service_id = s.service_id \n"
                    + "                    inner join medical_report as mr on mr.invoice_id=i.invoice_id and mr.service_id=s.service_id\n"
                    + "                   WHERE i.invoice_customer_id = ?  ;");
            pre.setString(1, cusID);

            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                if (reports.get(rs.getString(4)) != null) {
                    if (reports.get(rs.getString(4)).getDateAndTime().compareTo(rs.getDate(5)) > 0) {
                        reports.put(rs.getString(4), reports.get(rs.getString(4)));
                    }
                }
                reports.put(rs.getString(4), new MedicalReport(rs.getString(1), rs.getString(2), rs.getString(3), rs.getDate(5)));
            }
            for (String serID : reports.keySet()) {
                medicalReports.addReport(reports.get(serID));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return medicalReports.getReports();
    }

    public List<Service> getServicesByReservationID(String reservationID) {
        List<Service> services = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement(" select s.service_id,s.service_name,s.service_price,s.service_image,s.service_time\n"
                    + " from service_reservation as sr inner join service as s\n"
                    + " on s.service_id = sr.service_id where sr.reservation_id= ? ");
            pre.setString(1, reservationID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                services.add(new Service(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return services;
    }

    public List<Reservation> getReservationsByCusID(String cusID) {
        List<Reservation> reservations = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement("select  r.reservation_id, uc.user_name,uc.user_dob,uc.user_phone,uc.user_email,\n"
                    + "  ud.user_id as doctor_id ,ud.user_name as doctor_name ,\n"
                    + "  sl.slot_start_time,sl.slot_end_time,r.date_and_time,\n"
                    + "  r.status\n"
                    + "from reservation as r inner join user as uc \n"
                    + "on uc.user_id= r.reservation_customer_id inner join doctor_schedule as ds\n"
                    + "on r.reservation_doctor_schedule_id=ds.doctor_schedule_id inner join user as ud\n"
                    + "on ds.doctor_id=ud.user_id inner join slot as sl \n"
                    + "on sl.slot_id=ds.slot where r.reservation_customer_id= ?");
            pre.setString(1, cusID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                reservations.add(new Reservation(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
                        getServicesByReservationID(rs.getString(1)),
                        rs.getString(6), rs.getString(7),
                        rs.getString(8), rs.getString(9), rs.getDate(10),
                        rs.getString(11)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reservations;
    }

    public List<Invoice> getInvoicesByCusID(String cusID) {
        InvoiceList invoices = new InvoiceList();

        ProfileDAO dao = new ProfileDAO();
        try {
            PreparedStatement pre = connection.prepareStatement("SELECT i.invoice_id, nu.user_id AS nurse_id, nu.user_name AS nurse_name, nu.user_email AS nurse_email, "
                    + "cu.user_id AS customer_id, cu.user_name AS customer_name, "
                    + "du.user_id AS doctor_id, du.user_name AS doctor_name, "
                    + "slot.slot_start_time, slot.slot_end_time, i.date_and_time FROM invoice AS i "
                    + "INNER JOIN user AS nu ON nu.user_id = i.receptionist_id "
                    + "INNER JOIN user AS cu ON cu.user_id = i.invoice_customer_id "
                    + "INNER JOIN doctor_schedule AS ds ON i.invoice_doctor_schedule_id = ds.doctor_schedule_id "
                    + "INNER JOIN user AS du ON du.user_id = ds.doctor_id "
                    + "INNER JOIN slot ON slot.slot_id = ds.slot "
                    + "WHERE i.invoice_customer_id = ? ");

            pre.setString(1, cusID);
            ResultSet rs = pre.executeQuery();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            while (rs.next()) {
                Invoice i = new Invoice();
                List<MedicalReport> reports = dao.getMedicalReportsByCusIDAndInvoiceID(cusID, rs.getString(1));
                List<MedicalItem> medicalItems = dao.getMedicalItemsByCusIDAndInvoiceID(cusID, rs.getString(1));
                i.setInvoiceID(rs.getString(1));
                i.setNurseID(rs.getString(2));
                i.setNurseName(rs.getString(3));
                i.setNurseEmail(rs.getString(4));
                i.setReports(reports);
                i.setMedicalItems(medicalItems);
                i.setCusID(rs.getString(5));
                i.setCusName(rs.getString(6));
                i.setDoctorID(rs.getString(7));
                i.setDoctorName(rs.getString(8));
                i.setServiceStartTime(rs.getString(9));
                i.setServiceEndTime(rs.getString(10));
                try {
                    i.setDateAndTime(dateFormat.parse(rs.getString("date_and_time")));
                } catch (ParseException ex) {
                    Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
                i.setTotalMoney();

                invoices.addInvoice(i);

            }

        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return invoices.getInvoices();

    }

    public static void main(String[] args) {
        ProfileDAO dao = new ProfileDAO();

//        System.out.println(dao.getMedicalReportList("1"));
        System.out.println(dao.getReservationsByCusID("1"));
    }

}
