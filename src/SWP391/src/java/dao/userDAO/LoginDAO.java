/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.userDAO;

import dbcontext.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author Admin
 */
public class LoginDAO extends DBContext {

    public User getUserByEmailAndPassword(String user_email, String user_password) {
        try {
            PreparedStatement pre = connection.prepareStatement("SELECT * FROM user WHERE user_email = ? and user_password = ?");
            pre.setString(1, user_email);
            pre.setString(2, user_password);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                return new User(String.valueOf(rs.getInt(1)), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10),rs.getString(11));
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public User getUserByEmailAndAccountType(String user_email,String accountType ) {
        try {
            PreparedStatement pre = connection.prepareStatement("SELECT * FROM user WHERE user_email = ? and account_type= ? ");
            pre.setString(1, user_email);
            pre.setString(2, accountType);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                return new User(String.valueOf(rs.getInt(1)), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10),rs.getString(11));
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public static void main(String[] args) {
        LoginDAO dao = new LoginDAO();
        System.out.println(dao.getUserByEmailAndAccountType("sonnhhe163733@fpt.edu.vn","google"));
    }
}
