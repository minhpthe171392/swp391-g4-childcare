/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.userDAO;


import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 *
 * @author Admin
 */
public class DoctorProfileDAO extends dbcontext.DBContext{
    Connection conn;
    PreparedStatement ps;
    ResultSet   rs;
    
    public List<User> getAllDoctor() {
        List<User> list = new ArrayList<>();
        String query = "select user_id, user_email, user_name, user_avatar_url"
                + " from childcare.user where user_role='3'";
        try {
             conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
             rs = ps.executeQuery();
           while(rs.next()){
               String id = rs.getString(1);
               String email = rs.getString(2);
               String name = rs.getString(3);
               String avatarUrl = rs.getString(4);
              list.add(new User(id, email, name, avatarUrl));
           }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        return list;
    }
    
    
    public User getDoctorProfile(String uid) {
        
        String query = "select user_id, user_email, user_name, user_avatar_url from childcare.user where user_id = ?";
              
        try {
             conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
             ps.setString(1, uid);
             rs = ps.executeQuery();
           while(rs.next()){
               return new User(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4)
               );
           }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        return null;
    }
    
     
     public static void main(String[] args) {
        DoctorProfileDAO dao = new DoctorProfileDAO();
          User list  = dao.getDoctorProfile("1");
        
               System.out.println(list);     
         
     
}
}
