/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.invoice;

import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Invoice;
import model.Service;

/**
 *
 * @author Admin
 */
public class InvoiceDAO extends DBContext {

    /*
    author: MinhPT
    created on: 7/19/23
    description: get Invoice with status 0
     */
    public List<Invoice> getAllProcessingInvoice() {
        Connection cnn;
        PreparedStatement pstm;
        ResultSet rs;
        List<Invoice> invList = new ArrayList<>();
        try {
            String strSelect = "select invoice_id, user_name "
                    + "from invoice join user on invoice_customer_id = user_id "
                    + "where invoice_status = 0 ";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                String invoiceId = rs.getString(1);
                String customerName = rs.getString(2);
                invList.add(new Invoice(invoiceId, customerName));
            }
        } catch (Exception e) {
            System.out.println("getAllProcessingInvoice: " + e.getMessage());
        }
        return invList;
    }

    /*
    author: MinhPT
    created on: 7/19/23
    description: get Invoice detail
     */
    public Invoice getInvoiceDetail(String id) {
        Connection cnn;
        PreparedStatement pstm;
        ResultSet rs;
        Invoice inv = new Invoice();
        try {
            String strSelect = "select invoice_id, user_name "
                    + "from invoice join user on invoice_customer_id = user_id "
                    + "where invoice_id = ? ";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strSelect);
            pstm.setString(1, id);
            rs = pstm.executeQuery();
            while (rs.next()) {
                String invoiceId = rs.getString(1);
                String customerName = rs.getString(2);
                inv = new Invoice(invoiceId, customerName);
            }
        } catch (Exception e) {
            System.out.println("getAllProcessingInvoice: " + e.getMessage());
        }
        return inv;
    }

    public List<Service> getServicesList() {
        List<Service> services = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement("select service_id, service_name, service_price from service");
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                services.add(new Service(rs.getString(1), rs.getString(2), rs.getString(3)));
            }
        } catch (Exception ex) {
            System.out.println("getServicesByCategory" + ex.getMessage());
        }

        return services;
    }
    
    public List<Service> getInvoiceService(String invId) {
        List<Service> services = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement("select service_id "
                    + "from invoice join service_invoice where invoice.invoice_id=?");
            pre.setString(1, invId);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                services.add(new Service(rs.getString(1)));
            }
        } catch (Exception ex) {
            System.out.println("getServicesByCategory" + ex.getMessage());
        }

        return services;
    }

    public void doneInvoice(String conclusion, String invId) {
        Connection cnn;
        PreparedStatement pstm;
        try {
            String strDelete = "UPDATE `childcare`.`invoice` SET `invoice_conclusion` = ?,  `invoice_status` = 1 where invoice_id = ?";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strDelete);
            pstm.setString(1,conclusion);
            pstm.setString(2,invId);
            pstm.execute();
        } catch (Exception e) {
            System.out.println("doneInvoice: " + e.getMessage());
        }
    }

    public void saveInvoice(String invId, String serviceId) {
        Connection cnn;
        PreparedStatement pstm;
        try {
            String strDelete = "INSERT INTO `childcare`.`service_invoice` (`invoice_id`, `service_id`) VALUES (?, ?)";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strDelete);
            pstm.setInt(1, Integer.parseInt(invId));
            pstm.setInt(2, Integer.parseInt(serviceId));
            pstm.execute();
        } catch (Exception e) {
            System.out.println("saveInvoice: " + e.getMessage());
        }
    }
}
