/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.invoice;

import dao.medical_report.MedicalReportsDAO;
import dao.user.ProfileDAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Invoice;
import model.MedicineItem;
import model.MedicalReport;
import model.Medicine;
import model.InvoiceList;

/**
 *
 * @author Son Nguyen
 */
public class InvoiceListDAO extends dbcontext.DBContext {

    public List<MedicineItem> getMedicineItemsByCusIDAndInvoiceID(String cusID, String invoiceID) {
        List<MedicineItem> medicalItems = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement("SELECT i.invoice_id,"
                    + " m.medicine_id, m.medicine_name, m.medicine_price, m.medicine_quantity, m.medicine_image,"
                    + " mi.medicine_quantity as medicine_item_quantity "
                    + "FROM invoice AS i "
                    + "INNER JOIN medical_invoice AS mi ON i.invoice_id = mi.invoice_id "
                    + "INNER JOIN medicine AS m ON mi.medicine_id = m.medicine_id "
                    + "WHERE i.invoice_customer_id = ? AND i.invoice_id = ?");
            pre.setString(1, cusID);
            pre.setString(2, invoiceID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                medicalItems.add(new MedicineItem(invoiceID,
                        new Medicine(rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getString(6)),
                        rs.getInt(7)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return medicalItems;
    }

    public List<Invoice> getInvoicesByCusID(String cusID) {
        model.InvoiceList invoices = new model.InvoiceList();

        MedicalReportsDAO dao = new MedicalReportsDAO();

        try {
            PreparedStatement pre = connection.prepareStatement("SELECT i.invoice_id, nu.user_id AS nurse_id, nu.user_name AS nurse_name, nu.user_email AS nurse_email, "
                    + "cu.user_id AS customer_id, cu.user_name AS customer_name, "
                    + "du.user_id AS doctor_id, du.user_name AS doctor_name, "
                    + "slot.slot_start_time, slot.slot_end_time, i.date_and_time FROM invoice AS i "
                    + "INNER JOIN user AS nu ON nu.user_id = i.receptionist_id "
                    + "INNER JOIN user AS cu ON cu.user_id = i.invoice_customer_id "
                    + "INNER JOIN doctor_schedule AS ds ON i.invoice_doctor_schedule_id = ds.doctor_schedule_id "
                    + "INNER JOIN user AS du ON du.user_id = ds.doctor_id "
                    + "INNER JOIN slot ON slot.slot_id = ds.slot "
                    + "WHERE i.invoice_customer_id = ? ");

            pre.setString(1, cusID);
            ResultSet rs = pre.executeQuery();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            while (rs.next()) {
                Invoice i = new Invoice();
                List<MedicalReport> reports = dao.getMedicalReportsByCusIDAndInvoiceID(cusID, rs.getString(1));
                List<MedicineItem> medicineItems = getMedicineItemsByCusIDAndInvoiceID(cusID, rs.getString(1));
                i.setInvoiceID(rs.getString(1));
                i.setNurseID(rs.getString(2));
                i.setNurseName(rs.getString(3));
                i.setNurseEmail(rs.getString(4));
                i.setReports(reports);
                i.setMedicineItems(medicineItems);
                i.setCusID(rs.getString(5));
                i.setCusName(rs.getString(6));
                i.setDoctorID(rs.getString(7));
                i.setDoctorName(rs.getString(8));
                i.setServiceStartTime(rs.getString(9));
                i.setServiceEndTime(rs.getString(10));
                try {
                    i.setDateAndTime(dateFormat.parse(rs.getString("date_and_time")));
                } catch (ParseException ex) {
                    Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
                i.setTotalMoney();

                invoices.addInvoice(i);

            }

        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return invoices.getInvoices();

    }

    public Invoice searchInvoiceByCusIDAndInvoiceID(String cusID, String invoiceID) {
        for (Invoice i : getInvoicesByCusID(cusID)) {
            if (i.getInvoiceID().equals(invoiceID)) {
                return i;
            }
        }
        return null;
    }

    public List<Invoice> filterInvoicesByCusIDAndSortPrice(String cusID, String sortPrice) {
        InvoiceList invoices = new InvoiceList(getInvoicesByCusID(cusID));
        return invoices.sortByTotalMoney(sortPrice);
    }

    public List<Invoice> filterInvoicesByCusIDAndAppearance(String cusID, String appearance) {
        InvoiceList invoices = new InvoiceList(getInvoicesByCusID(cusID));
        return invoices.sortByInvoiceAppearance(appearance);
    }

    public List<Invoice> searchInvoicesByCusIDAndServiceNameOrDoctorName(String cusID, String searchName) {
        InvoiceList invoices = new InvoiceList(getInvoicesByCusID(cusID));
        InvoiceList invoicesSearchByServiceNameOrDoctorName = new InvoiceList();
        for (Invoice i : invoices.getInvoices()) {
            if (i.getDoctorName().contains(searchName)) {
                invoicesSearchByServiceNameOrDoctorName.addInvoice(i);
            }
            for (MedicalReport mr : i.getReports()) {
                if (mr.getService().getName().contains(searchName)) {
                    invoicesSearchByServiceNameOrDoctorName.addInvoice(i);
                }
            }
        }
        return invoicesSearchByServiceNameOrDoctorName.getInvoices();
    }

}
