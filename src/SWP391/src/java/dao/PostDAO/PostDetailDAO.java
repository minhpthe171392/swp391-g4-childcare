/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.PostDAO;

import dao.userDAO.DoctorProfileDAO;
import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Post;
import model.User;

/**
 *
 * @author Admin
 */
public class PostDetailDAO {
     Connection conn;
    PreparedStatement ps;
    ResultSet rs;
    
    public Post getDetaiPostlByPostId(String id) {
       
        String query = "SELECT * FROM childcare.post where post_id = ?";
        try {
            conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
             ps.setString(1, id);
             rs = ps.executeQuery();
           while(rs.next()){
               return new Post(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6));
           }
        } 
        catch (Exception e) {
        }
        
        return null;
    }
      public static void main(String[] args) {
        PostDetailDAO dao = new PostDetailDAO();
        Post list = dao.getDetaiPostlByPostId("1");
         
               System.out.println(list);     
         }
     }

