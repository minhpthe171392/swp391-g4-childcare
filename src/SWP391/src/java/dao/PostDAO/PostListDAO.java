/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.PostDAO;

import dao.categoryDAO.CategoryDAO;
import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Category;
import model.Post;

/**
 *
 * @author Admin
 */
public class PostListDAO extends dbcontext.DBContext {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public List<Post> getPostList() {
        List<Post> list = new ArrayList<>();
        String query = "SELECT * FROM childcare.post";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Post(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3).substring(0, 10)+"...",
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6)
                )
                );
            }
        } catch (Exception e) {
            System.out.println("getPostList: "+e.getMessage());
        }

        return list;
    }
    
     public static void main(String[] args) {
        PostListDAO dao = new PostListDAO();
        List<Post> list = dao.getPostList();
         for (Post o : list) {
               System.out.println(o);     
         }
            
            
        
    }

}
