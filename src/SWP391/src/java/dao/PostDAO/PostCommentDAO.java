/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.PostDAO;

import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import model.Comments;
import model.Post;
import model.User;

/**
 *
 * @author LT
 */
public class PostCommentDAO {
    Connection conn;
    PreparedStatement ps;
    ResultSet rs;
    
    
    
     public void insertComment(String comment )
           {
        String query = "INSERT INTO comments (comment_text) VALUES (?)";
        try {
            System.out.println(comment);
            conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
             ps.setString(1, comment);
             ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
     public List<Comments> getComment(){
          List<Comments> list = new ArrayList<>();      
        String query = "select * from childcare.comments";
        try {
            conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()){
             list.add(new Comments(rs.getString(1),
                                   rs.getString(2)));
           }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
          return list;
     }

     
     public static void main(String[] args) {
    String comment = "bach";
    PostCommentDAO yourObject = new PostCommentDAO();

    
    yourObject.insertComment(comment);

    
    System.out.println("Comment inserted successfully!");
}
//      public static void main(String[] args) {
//        PostCommentDAO dao = new PostCommentDAO();
//        List<Comments> list = dao.getComment();
//         
//               System.out.println(list);     
//         
//            
//            
//        
//    }
}
