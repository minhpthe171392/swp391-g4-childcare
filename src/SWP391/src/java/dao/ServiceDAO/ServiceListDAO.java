/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.ServiceDAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Service;

/**
 *
 * @author Admin
 */
public class ServiceListDAO extends dbcontext.DBContext{
   public List<Service> getServicesByCategory(String categoryID){
       List<Service> services= new ArrayList<>();
       try {
           PreparedStatement pre=connection.prepareStatement("select * from service"+(categoryID!=null?" where category_id= ?":""));
             if(categoryID!=null){
                 pre.setString(1, categoryID);
             }
             ResultSet rs=pre.executeQuery();
             while(rs.next()){
                   services.add(new Service(rs.getString(1), rs.getString(2), rs.getString(4), rs.getString(6),
                           rs.getString(5), rs.getString("service_time")));
             }
       } catch (SQLException ex) {
           Logger.getLogger(ServiceListDAO.class.getName()).log(Level.SEVERE, null, ex);
       }
    
      return services;
}
   
     public static void main(String[] args) {
        ServiceListDAO d=new ServiceListDAO();
         System.out.println(d.getServicesByCategory("1"));
                
    }
}
