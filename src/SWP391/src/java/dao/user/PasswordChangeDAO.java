/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.user;

import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.User;

/**
 *
 * @author Admin
 */
public class PasswordChangeDAO extends DBContext {

    Connection cnn;
    PreparedStatement pstm;
    ResultSet rs;

    public void updatePassword(String newPass) {
        try {
            String strUpdate="update user set user_password=? where user_id=?";
            cnn = new DBContext().connection;
            pstm=cnn.prepareStatement(strUpdate);
            pstm.setString(1,newPass);
            pstm.setInt(2, Integer.parseInt("1"));
            pstm.execute();
        } catch (Exception e) {
            System.out.println("Update: " + e.getMessage());
        }
    }

    public boolean PasswordVerification(User u, String inputPass) {
        try {
            String strSelect = "select user_password from user where user_id=? ";
            cnn = new DBContext().connection;
            if (cnn != null) {
                System.out.println("Connect success");
            } else {
                System.out.println("Connect fail");
            }
            pstm = cnn.prepareStatement(strSelect);
            pstm.setString(1,u.getId());
            rs = pstm.executeQuery();
            while (rs.next()) {
                String curPass = rs.getString(1);
                if (inputPass.equals(curPass)){
                    return true;
                }
            }
        } catch (Exception e) {
            System.out.println("PasswordVerification: " + e.getMessage());
        }
        return false;
    }

    public void updatePassword(User u, String newPass) {
        try {
            String strUpdate="update user set user_password=? where user_id=?";
            cnn = new DBContext().connection;
            if (cnn != null) {
                System.out.println("Connect success");
            } else {
                System.out.println("Connect fail");
            }
            pstm=cnn.prepareStatement(strUpdate);
            pstm.setString(1,newPass);
            pstm.setInt(2, Integer.parseInt(u.getId()));
            pstm.execute();
        } catch (Exception e) {
            System.out.println("Update: " + e.getMessage());
        }
    }
}
