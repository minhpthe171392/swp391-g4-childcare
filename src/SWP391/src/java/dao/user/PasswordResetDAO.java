/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.user;

import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.User;

/**
 *
 * @author Admin
 */
public class PasswordResetDAO {

    Connection cnn;
    PreparedStatement pstm;
    ResultSet rs;

    public User FindUserByIdentifier(String iden) {
        System.out.println("12345");
        User u = new User();
        try {
            String strSelect = "select user_id, user_name, user_email, user_phone"
                    + " from user where user_email=? or user_phone=?";
            cnn = new DBContext().connection;
            if (cnn != null) {
                System.out.println("Connect success");
            } else {
                System.out.println("Connect fail");
            }
            pstm = cnn.prepareStatement(strSelect);
            pstm.setString(1, iden);
            pstm.setString(2, iden);
            rs = pstm.executeQuery();

            while (rs.next()) {
                String id = rs.getString(1);
                String name = rs.getString(2);
                String email = rs.getString(3);
                String phone = rs.getString(4);
                u = new User(id, name, email, phone);
            }
        } catch (Exception e) {
            System.out.println("FindUserByIdentifier: " + e.getMessage());
        }
        return u;
    }

}
