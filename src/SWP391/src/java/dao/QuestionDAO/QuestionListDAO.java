/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.QuestionDAO;

import dao.PostDAO.PostListDAO;
import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Post;
import model.Question;

/**
 *
 * @author LT
 */
public class QuestionListDAO {
    Connection conn;
    PreparedStatement ps;
    ResultSet rs;
    
     public List<Question> getQuestionList() {
        List<Question> list = new ArrayList<>();
        String query = "SELECT * FROM childcare.question";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Question(
                        rs.getInt(1),
                        rs.getDate(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6)
                )
                );
            }
        } catch (Exception e) {
            System.out.println("getQuestionList "+e.getMessage());
        }

        return list;
    }
     
     public void insertQuestion(String name, String email, String title, String description )
           {
        String query = "INSERT INTO question (question_name, question_email, question_title, question_description) VALUES (?, ?, ?, ?)";
        try {
            System.out.println(name+email+title+description);
            conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
             ps.setString(1, name);
              ps.setString(2, email);
               ps.setString(3, title);
                ps.setString(4, description);
             ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
       public List<Question> displayTopQuestion() {
        List<Question> list = new ArrayList<>();
        String query = "SELECT * FROM question ORDER BY question_date DESC LIMIT 4";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Question(
                        rs.getInt(1),
                        rs.getDate(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6)
                )
                );
            }
        } catch (Exception e) {
            System.out.println("getQuestionList "+e.getMessage());
        }

        return list;
    }
//    public static void main(String[] args) {
//        QuestionListDAO dao = new QuestionListDAO();
//        List<Question> list = dao.getQuestionList();
//         for (Question o : list) {
//               System.out.println(o);     
//         }
//    }

    public static void main(String[] args) {
        String name = "ndc";
        String email = "ndc@example.com";
        String title = "Question Title";
        String description = "Question Description";

        QuestionListDAO dao = new  QuestionListDAO();
        dao.insertQuestion(name, email, title, description);
        
       
    }

}
