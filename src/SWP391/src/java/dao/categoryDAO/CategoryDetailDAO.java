/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.categoryDAO;

import dao.PostDAO.PostDetailDAO;
import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Category;
import model.Post;

/**
 *
 * @author LT
 */
public class CategoryDetailDAO {
     Connection conn;
    PreparedStatement ps;
    ResultSet rs;
    
    public Category getDetaiCategorylById(String cid) {
       
        String query = "SELECT * FROM childcare.category where category_id = ?";
        try {
             conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
             ps.setString(1, cid);
             rs = ps.executeQuery();
           while(rs.next()){
               return new Category(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4)
               );
           }
        } 
        catch (Exception e) {
        }
        
        return null;
    }
      public static void main(String[] args) {
        CategoryDetailDAO dao = new CategoryDetailDAO();
        Category list = dao.getDetaiCategorylById("3");
         
               System.out.println(list);     
         }
}
