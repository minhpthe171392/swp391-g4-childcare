/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.categoryDAO;

import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;

/**
 *
 * @author Admin
 */
public class CategoryDAO extends DBContext{
     Connection conn;
    PreparedStatement ps;
    ResultSet   rs;
    
     public List<Category> getAllCategory() {
        List<Category> list = new ArrayList<>();
        String query = "select * from childcare.category";
        try {
             conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
             rs = ps.executeQuery();
           while(rs.next()){
               list.add(new Category(
                       rs.getInt(1),
                       rs.getString(2),
                       rs.getString(3),
                       rs.getString(4)));
           }
        } catch (Exception e) {
        }
        
        return list;
    }
     public Category getCategoryByID(String id){
         try {
             PreparedStatement pre=connection.prepareStatement("select * from category where category_id = ?");
             pre.setString(1, id);
           ResultSet rs= pre.executeQuery();
           while(rs.next()){
               return new Category(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
           }
         } catch (SQLException ex) {
             Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
     }
     
     public static void main(String[] args) {
        CategoryDAO dao = new CategoryDAO();
        List<Category> list = dao.getAllCategory();
         for (Category o : list) {
               System.out.println(o);     
         }
            
              System.out.println(dao.getCategoryByID("1"));
        
    }
}
